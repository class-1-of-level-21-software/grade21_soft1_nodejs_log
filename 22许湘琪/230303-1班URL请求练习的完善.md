# 完善URL请求练习

## app.js
```js
'use strict'

//引用模块
const Koa=require('koa');


// let filtes=fs.readdirSync('./controller')//用同步获取控制器文件夹中的文件
// console.log(filtes)
// let filt=filtes.filter(item=>{
//     return item!=='index.js'//排除index.js文件
// })
// console.log(filt)

// filt.forEach(item=>{
//     let obj=require('./controller/'+item.replace('.js',''))//调用的路由模块
//     console.log('修改后:'+obj)

//     for(let key in obj){
//         // console.log(key)
//         let ad=key.split(' ')//将字符串分割成数组
//         // console.log('是'+ad)
//         let method=ad[0]//请求方法
//         let url=ad[1]//请求路径
//         let fn=obj[key]//函数方法
    
//         if(method==='get'){
//             router.get(url,fn)
//         }else if(method==='post'){
//             router.post(url,fn)
//         }else if(method==='put'){
//             router.put(url,fn)
//         }else if(method==='delete'){
//             router.delete(url,fn)
//         }
//     }
// })

//引入index.js的模块
let index=require('./controller')


//实例化
let app=new Koa();

index(app);
// console.log(obj)


//监视端口
let posh=8000;
app.listen(posh);

console.log(`网站:http://localhost:${posh}`);
```

## index.js
```js
'use strict'
//模块
const fs = require('fs');

//koa-router:是函数调用
const router = require('koa-router')();
//koa-bodyparser不是函数调用
const bodyParser = require('koa-bodyparser')



//请求的方法函数:注册指定模块的所有定义路由
function fn_url(obj) {
    for (let key in obj) {
        // console.log(key)
        let ad = key.split(' ')//将字符串分割成数组
        // console.log('是'+ad)
        let method = ad[0]//请求方法
        let url = ad[1]//请求路径
        let fn = obj[key]//函数方法

        if (method === 'get') {
            router.get(url, fn)
        } else if (method === 'post') {
            router.post(url, fn)
        } else if (method === 'put') {
            router.put(url, fn)
        } else if (method === 'delete') {
            router.delete(url, fn)
        }
    }
}

//判断请求路径的方法函数
function url(app) {
    let filtes = fs.readdirSync('./controller')//用同步获取控制器文件夹中的文件
    console.log(filtes)
    let filt = filtes.filter(item => {
        return item !== 'index.js'//排除index.js文件
    })

    filt.forEach(item => {
        //replace() 方法用于在字符串中用一些字符替换另一些字符，或替换一个与正则表达式匹配的子串。
        let obj = require('./' + item.replace('.js', ''))//调用的路由模块
        console.log('修改后:' + obj)

        fn_url(obj);
    })

    app.use(bodyParser())
    app.use(router.routes())
}

console.log(url)
module.exports = url;
```

## api.http
```js
@url=http://localhost:8000


### 获取所有学生
GET {{url}}/student HTTP/1.1

### 获取对应id的同学
GET {{url}}/student/3 HTTP/1.1

### 测试
GET {{url}}/teacher/2 HTTP/1.1

### 
GET {{url}}/school HTTP/1.1
```
![1](./img/230303/1.png)
![2](./img/230303/2.png)
![3](./img/230303/3.png)
![4](./img/230303/4.png)