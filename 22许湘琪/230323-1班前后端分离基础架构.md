# 前后端分离基础架构

## 一.后端:back_end文件夹

### bontrollers控制器文件夹

+ index.js
```js
'use strict'

//引入模块
const router=require('koa-router')();
const bodyparser=require('koa-bodyparser')
const {fn_files,fn_Url}=require('../utils/tools')

function fn_controller(app){

    //获取控制器中的所有文件
    let files=fn_files();
    console.log(files)

    //遍历控制器文件，批量注册路由
    fn_Url(files,router)

    app.use(bodyparser())

    return router.routes()
}

//暴露模块方法
module.exports=fn_controller
```
+ roles.js
```js
'use strict'

//查找所有信息
async function getAll(ctx,next){
    ctx.body='鸡腿'
}

//查找指定id的信息
async function getById(ctx,next){

}

//新增
async function addItem(ctx,next){

}

//修改
async function updateItem(ctx,next){

}

//删除
async function deleteItem(ctx,next){

}

module.exports={
    'get /roles':getAll,
    'get /roles/:id':getById,
    'put /roles':addItem,
    'post /roles/:id':updateItem,
    'delete /roles/:id':deleteItem
}
```

### model文件夹

+ index.js
```js
'usr strict'

const { Sequelize } = require('sequelize')

let db = new Sequelize('db1', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql'
})

const { fn_files } = require('../utils/tools')

let file = fn_files('./model')

//定义一个对象变量存储
let obj = {}

file.forEach(item => {
    let modelObj = require('../model/' + item)//接收暴露的数据类型
    let name = item.replace('.js', '')//去后缀
    let lowName = name.toLowerCase();//将字符变为小写的

    obj[name] = db.define(lowName, modelObj, {
        tableName: `app_` + lowName
    })
})

obj.sync=async (force)=>{
    if(force){
        return db.sync({force:true})
    }
    return db.sync()
}

module.exports=obj

```

+ Roles.js
```js
'use strict'
const { DataTypes } = require('sequelize')

let obj = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    role_name: DataTypes.STRING,
    role_site: DataTypes.STRING
}

//暴露
module.exports=obj
```

### utils工具文件夹
+ tools.js
```js
'use strict'

//引用fs模块，对文件进行处理
const fs=require('fs')

//找到所有的控制器文件
function fn_files(path) {
    path = path || './controllers'

    //找到控制器文件夹下的所有文件
    let files=fs.readdirSync(path)

    return files.filter(item=>{
        //排除index.js和后缀非.js的文件
        return item!=='index.js' && item.endsWith('.js')
    })
}

//遍历控制器文件，注册路由
function fn_Url(file,router) {
    file.forEach(item=>{
        let obj=require('../controllers/'+item)

        for(let key in obj){
            let files=key.split(' ')
            let urlname=files[0]//请求方法
            let url=files[1]//请求路径
            let fn=obj[key]//请求方法

            if(urlname==='get'){
                router.get(url,fn)
            }else if(urlname==='put'){
                router.put(url,fn)
            }else if(urlname==='post'){
                router.post(url,fn)
            }else if(urlname==='delete'){
                router.delete(url,fn)
            }
        }
    })

}

module.exports = {
    fn_files,
    fn_Url
}
```
### app.js
```js
'use strict'

//引用模块
const Koa=require('koa')
//实例化
let app=new Koa()

//测试数据库,测试成功
// const model=require('./model')
// model.sync(true).then(()=>{
//     model.Roles.create({
//         role_name:'鸡腿',
//         role_site:'炸鸡店'
//     })
// })

//注册路由
let fn_controller=require('./controllers')
app.use(fn_controller(app))


//监听端口
let post=3030;
app.listen(post)
console.log(`http://localhost:${post}`)
```
![1](./img/230323/1.png)
![2](./img/230323/2.png)

## 二.前端:front_end文件夹
### statics文件

+ js文件夹
```js
    + role.js
    //新增
function butPost(){
    location.href = 'addOrEdit.html';
}

//编辑
function butPut(){
    location.href = 'addOrEdit.html';
}

//删除
function butDel(){
    if(confirm('你确定要删除吗?')){

    }
}


+ roleEdit.js

    'use strict'
//保存
function butSave(){

}


//返回
function butCanel(){
    location.href='index.html'
}
```
+ css文件夹

    + base.css
```js
    /* 边框的显示 */
table,
th,
tr,
td {
    border: solid 1px black;
}

/* 去除边框的边距 */
table {
    border-collapse: collapse;
}

/* 设置外边距 */
div{
    margin-bottom: 10px;
}
```

### index.html
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>首页</title>
    <link rel="stylesheet" href="./statics/css/base.css">
</head>
<body>
    <div>
        <input type="text" placeholder="请输入关键字查找">
        <input type="button" value="查询">
        <input type="button" value="新增" onclick="butPost()">
    </div>
    <div>
        <table>
        <tr>
            <th>编号</th>
            <th>名称</th>
            <th>地址</th>
            <th>操作</th>
        </tr>
        <tr>
            <td>1</td>
            <td>鸡腿</td>
            <td>炸鸡店</td>
            <td>
                <input type="button" value="编辑" onclick="butPut()">
                <input type="button" value="删除" onclick="butDel()">
            </td>
        </tr>
    </table>
    </div>
        <script src="./statics/js/role.js"></script>
</body>
</html>
```

### addOrEdit.html
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>新增或修改</title>
</head>
<body>
    <table>
        <tr>
            <td>名称</td>
            <td><input type="text" name="rolename"></td>
        </tr>
        <tr>
            <td>地址</td>
            <td><input type="text" name="rolesite"></td>
        </tr>

        <tr>
            <td><input type="button" value="提交" onclick="butSave()"></td>
            <td><input type="button" value="返回" onclick="butCanel()"></td>
        </tr>
    </table>

    <script src="./statics/js/roleEdit.js"></script>
</body>
</html>
```
![3](./img/230323/3.png)
![4](./img/230323/4.png)