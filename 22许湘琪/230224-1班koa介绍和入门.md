# koa介绍和入门

## 一.Web开发

### Web开发经历的阶段:
+ 静态Web页面：由文本编辑器直接编辑并生成静态的HTML页面，如果要修改Web页面的内容，就需要再次编辑HTML源文件，早期的互联网Web页面就是静态的；

+ CGI：由于静态Web页面无法与用户交互，比如用户填写了一个注册表单，静态Web页面就无法处理。要处理用户发送的动态数据，出现了Common Gateway Interface，简称CGI，用C/C++编写。

+ ASP/JSP/PHP：由于Web应用特点是修改频繁，用C/C++这样的低级语言非常不适合Web开发，而脚本语言由于开发效率高，与HTML结合紧密，因此，迅速取代了CGI模式。ASP是微软推出的用VBScript脚本编程的Web开发技术，而JSP用Java来编写脚本，PHP本身则是开源的脚本语言。

+ MVC：为了解决直接用脚本语言嵌入HTML导致的可维护性差的问题，Web应用也引入了Model-View-Controller的模式，来简化Web开发。ASP发展为ASP.Net，JSP和PHP也有一大堆MVC框架。

### 用Node.js开发Web服务器端的优势:
+ 一是后端语言也是JavaScript，以前掌握了前端JavaScript的开发人员，现在可以同时编写后端代码；

+ 二是前后端统一使用JavaScript，就没有切换语言的障碍了；

+ 三是速度快，非常快！这得益于Node.js天生是异步的。

## 二.koa

+ koa是Express的下一代基于Node.js的web框架

+ 有1.0和2.0两个版本。但与koa1相比，koa2完全使用Promise并配合async来实现异步。

### 安装koa的包

在命令终端中输入
```js
    //一般用yarn进行包管理，但是不强求
    方法一: yarn add koa
```
    这个方法安装时若报这个错误:yarn : 无法加载文件 C:\Users\Administrator\AppData\Roaming\npm\yarn.ps1，因为在此系统上禁止 
运行脚本。

    按win+x打开管理员的命令框进行操作

    具有方法的网站:https://blog.csdn.net/qq_39290878/article/details/109738433
    

```js

    方法二:npm install koa/npm add koa
```


+ npm会把koa2以及koa2依赖的所有包全部安装到当前目录的node_modules目录下。

+ 忽略文件夹要创建仓库，有仓库才有忽略

+ 目录的node_modules目录是要被忽略的，不会上传到仓库上
```js

git init 创建空仓库的命令

```
1. 添加一个忽略文件 **.gitignore**

2. 在文件中输入**node_modules**目录名
```js
拉取忽略的文件夹的命令:yarn
```


### koa的运用

+ koa将调用我们传入的异步函数来处理每一个http请求

+ 参数ctx是由koa传入的封装了request和response的变量，我们可以通过它访问request和response

+ next是koa传入的将要处理的下一个异步函数。
```js
'use strict'

//1.导入Koa

let Koa=require('koa')

//2.new一个koa实例
let app=new Koa();

//3.use处理请求
app.use(async(ctx,next)=>{
    await next();
    ctx.response.type='text/html'
    ctx.response.boby='<p>鸡腿<p>'
})

//4.监听运行

let port =4000;

app.listen(port)

console.log(`服务:http://localhost:${port}`)
```
