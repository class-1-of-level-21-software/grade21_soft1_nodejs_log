# 热重载和nunjucks模板引擎

## 一.热重载:nodemon

nodemon 的详细介绍:https://juejin.cn/post/7035266324670447623

### 安装

+ 方法一:全局安装
```js
$ npm i -g nodemon

删除: npm un -g nodemon
```

+ 方法二:本地安装
```js
$ npm i -D nodemon

注意：本地安装需要在 package.json 文件的 script 脚本中指定要需要执行的命令

{
  "script": {
    "dev": "nodemon app.js"
  }
}
使用 npm dev 运行
```

### 使用时报错

![1](./img/230307/1.png)

+ 解决方法: https://blog.csdn.net/tory886/article/details/119670783

![2](./img/230307/2.png)

+ 运行成功
![3](./img/230307/3.png)

## 二.nunjucks模板引擎

+ Nunjucks是js专用的，功能丰富的模板引擎，

+ 模板引擎就是基于模板配合数据构造出字符串输出的一个组件,

+ Nunjucks支持块级继承（block inheritance）、自动转义、宏（macro）、异步控制等等功能。

官方文档(中文版)的详细内容:https://nunjucks.bootcss.com/templating.html

### 安装nunjucks
```
    npm add nunjucks/yarn add nunjucks
```

### 用法

### nunjucks 插值语法

+ nunjucks 的插值语法和我们平常写 VUE 的语法一样, 只需要写双大括号{{}} 即可以完成插值引用.

### 渲染:render

+ render用于渲染网页，简单来说就是可以把数据传到模板里并使用，需要传入页面url，和传入的数据。

index.html
```js
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>吃饭</title>
</head>
<body>
    {{username}}吃鸡腿
</body>
</html>
```
users.js
```js
'use strict'

const nunjucks=require('nunjucks')

nunjucks.configure('views',{
    autoescape:true
})

async function getAll(ctx, next) {
    ctx.body=nunjucks.render('index.html',{username:"干饭人"})
}

async function getById(ctx, next) {

}

async function addItem(ctx, next) {

}

async function updateItem(ctx, next) {

}

async function delItem(ctx, next) {

}

module.exports = {
    'get /users': getAll,
    'get /users/:id': getById,
    'post /users': addItem,//新增
    'put /users/:id': updateItem,//修改
    'delete /users/:id': delItem,
}
```
![4](./img/230307/4.png)

### 循环:for in 循环

+ 开始和结束语句都要在 **{%  %}**中间

+ 要一个结束循环的标志. {% endfor %}

use.html
```js
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>生活</title>
</head>
<body>
    <ul>
        {% for item in items %}

        <!-- <li>{{item.id}}</li> -->

        <li>{{item.name}}</li>

        {% endfor %}
    </ul>
</body>
</html>
```
users,js
```js
'use strict'

const nunjucks=require('nunjucks')

nunjucks.configure('views',{
    autoescape:true
})

async function getAll(ctx, next) {
    ctx.body=nunjucks.render('use.html',{items:[
        {id:1,name:"烤鸡腿"},
        {id:2,name:"咖啡"},
        {id:3,name:"牛奶"},
        {id:4,name:"椰奶"},
    ]})
}

async function getById(ctx, next) {
    // let id = ctx.request.params.id;

    // let data = list.filter(item => {
    //     return item.id == id
    // })

    // ctx.body = data
}

async function addItem(ctx, next) {

}

async function updateItem(ctx, next) {

}

async function delItem(ctx, next) {

}

module.exports = {
    'get /users': getAll,
    'get /users/:id': getById,
    'post /users': addItem,//新增
    'put /users/:id': updateItem,//修改
    'delete /users/:id': delItem,
}
```
![5](./img/230307/5.png)

### nunjucks.configure()
```
传入 path 指定存放模板的目录，opts 可让某些功能开启或关闭，这两个变量都是可选的。path 的默认值为当前的工作目录，opts 提供以下功能：
```
+ autoescape (默认值: true) 控制输出是否被转义，查看 Autoescaping

+ throwOnUndefined (default: false) 当输出为 null 或 undefined 会抛出异常
+ trimBlocks (default: false) 自动去除 block/tag 后面的换行符
+ lstripBlocks (default: false) 自动去除 block/tag 签名的空格
+ watch (默认值: false) 当模板变化时重新加载。使用前请确保已安装可选依赖 chokidar。
+ noCache (default: false) 不使用缓存，每次都重新编译
+ web 浏览器模块的配置项
    + useCache (default: false) 是否使用缓存，否则会重新请求下载模板
    + async (default: false) 是否使用 ajax 异步下载模板
+ express 传入 express 实例初始化模板设置
+ tags: (默认值: see nunjucks syntax) 定义模板语法，查看 Customizing Syntax
```
configure 返回一个 Environment 实例, 他提供了简单的 api 添加过滤器 (filters) 和扩展 (extensions)
```