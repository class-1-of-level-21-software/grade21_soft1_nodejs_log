index.js
```js
'use strict';

const fs = require('fs');

const { Role } = require('../model');





// 希望给所有的ctx挂载上一个方法，这个方法和env.render效果一致
async function getAll(ctx, next) {
    let data = ctx.body = await Role.findAll();
    ctx.response.type = 'text/html';
    ctx.render('roles.html', { list: data });
}

async function getById(ctx, next) {
    // 拿到id
    let id = ctx.request.params.id;
    console.log(id);
    let role = await Role.findAll({
        where: {
            id: id
        }
    })
    let res = role.length > 0 ? role[0].dataValues : {}
    console.log(res);
    ctx.body = res;
    // console.log(ctx.kaixin);
}



async function addItem(ctx, next) {
    let obj = ctx.request.body;
    console.log(obj);
    // Role来自于模型文件的名称，同时也是模型名称
    Role.create({
        role_name: obj.rolename
    })


    ctx.body = '新增角色成功';
}
async function updateItem(ctx, next) {
    // 拿到id
    let id = ctx.request.params.id;
    let param = ctx.request.body;

    Role.update({ role_name: param.rolename }, {
        where: {
            id: id
        }
    });

    ctx.body = '更新成功'

}
async function delItem(ctx, next) {
    let id = ctx.request.params.id;
    Role.destroy({
        where: {
            id: id
        }
    })
    ctx.body = '删除成功';
}


async function roleEdit(ctx, next) {
    let id = ctx.request.params.id;
    let role = await Role.findAll({
        where: {
            id: id
        }
    })
    let res = role.length > 0 ? role[0].dataValues : {}

    ctx.render('addRole.html', {res})
}

module.exports = {
    'get /roles': getAll,
    'get /roleEdit/:id': roleEdit,
    'get /roles/:id': getById,
    'post /roles': addItem,
    'put /roles/:id': updateItem,
    'delete /roles/:id': delItem,
}
```
roles.html
```html
<body>
    <table>
        <tr>
            <th>主键Id</th>
            <th>角色名称</th>
            <th>备注</th>
            <th>操作</th>
        </tr>
        {% for item in list %}
        <tr id="{{item.id}}">
            <td>{{item.id}}</td>
            <td>{{item.role_name}}</td>
            <td>{{item.remarks}}</td>
            <td>
                <input type="button" value="编辑" onclick="redirectEditRole(`{{item.id}}`)">
                <input type="button" value="删除" onclick="delRole(`{{item.id}}`)">
            </td>
        </tr>
        {% endfor %}
    </table>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="../statics/js/ro les.js"></script>
</body>
```
addRole.html
```htm
  <table>
        <tr>
            <td>角色名称：</td>
            <td><input type="text" name="rolename" value="{{res.role_name}}"></td>
        </tr>
        <tr>
            <td><input type="button" value="提交保存" onclick="save()"></td>
            <td><input type="button" value="返回" onclick="cancle()"></td>
        </tr>

    </table>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="../statics/js/roles.js"></script>
 ```
roles.js
```js
// 当前函数，用于角色的删除功能
function delRole(id) {
    if (confirm(`你确定要删除id为${id}的角色吗？`)) {
        axios.delete('/roles/' + id).then(() => {
            let nn = $('#' + id);
            nn.remove();
        });
    }
}

function redirectEditRole(id) {
    location.href = '/roleEdit/'+id;
}

function save() {
    console.log('你保存');
}

function cancle() {
    location.href = '/roles'
}
```