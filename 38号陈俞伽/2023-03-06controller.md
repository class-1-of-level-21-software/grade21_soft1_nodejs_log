app.js
```js

let koa = require('koa');
let app = new koa();
let controller = require('./controller');


controller(app);

app.listen(8000);
console.log('http://localhost:8000');

```
index.js
```js
const router = require('koa-router')();
const bodyparser = require('koa-bodyparser');
const { findControllers, registryRouter } = require('../utils/tools');


function processRouter(app) {
    let conreollers = findControllers();
    registryRouter(conreollers,router)
    app.use(router.routes())
    app.use(bodyparser())
}

module.exports = processRouter;
```
users.js
```js
'use strict'

async function getAll(ctx,next){
    ctx.body = '<h1>天天学习</h1>'
}

async function getById(ctx,next){

}
async function addItem(ctx,next){
    
}
async function updateIetm(ctx,next){
    
}
async function delItem(ctx,next){
    
}


module.exports = {
    'get /users': getAll,
    'get /users/:id': getById,
    'post /users': addItem,
    'put /users/:id':updateIetm,
    'delete /users/:id':delItem
}

```
tools.js
```js
const fs = require('fs');
const Router = require('koa-router');

function findControllers(path) {
    path = path || '';
    console.log(path);
    let files = fs.readdirSync(path);
    return files.filter(item => {
        return item !== 'index.js';
    })
}

function registryRouter(conreollerFile) {
      conreollerFile.forEach(item => {
        let tmpmodule = require('../conreoller/'+item.require('.js',''));
       for(let key in tmpmodule){
            let tmpArr = key.split(' ');
            
            let method = tmpArr[0];
            let url = tmpArr[1];
            let fn = tmpmodule[key];

            if (method === 'get') {
                router.get(url,fn)
            }else if(method === 'post'){
                router.post(url,fn)
            }else if(method === 'put'){
                router.put(url,fn)
            }else if(method === 'delete'){
                router.delete(url,fn)
            }
       }
      });

}

module.exports = {
    findControllers,
    registryRouter
}
```