users.js
```js
async function getAll(ctx,next){
    ctx.reader('index.js',{})
}
module.exports = {
    'get /users': getAll,
}
```

app.js
```js
let koa = require('koa');
let app = new koa();
let controller = require('./controller');
let nun = require('./nunConfig')
 app.use(nun())
controller(app);

app.listen(8000);
```

nunConfig.js
```js
const nunjucks = require('nunjucks');

let env = nunjucks.configure('views',{
    autoescape : true,
    watch : true
})


function fn(){
    return async (ctx,next)=>{
         async (view,data) => 
        {
            ctx.body = 
                env.render(view,data)
        }
        await next();
    }
}
module.exports = fn ;
```
