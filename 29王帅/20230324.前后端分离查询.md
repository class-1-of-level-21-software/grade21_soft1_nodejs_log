## 前后端分离之查询

### 前端

init.js
```
'use strict'

$(function(){
    $.get('http://localhost:8080/product',function(result){
        // console.log(result);
        let tb =$('table');
        console.log(tb);
        result.forEach(item => {
            let html =`
                        <tr>
                            <td>${item.id}</td>
                            <td>${item.productName}</td>
                            <td>${item.price}</td>
                            <td>${item.stockNum}</td>
                            <td>${item.suppiler}</td>
                            <td>不平常的备注</td>
                            <td><input type="button" value="详情" onclick="detail(${item.id})"></td>
                        </tr>
            `
            tb.append(html);
        });
    });
});
function detail(id){
    console.log(id);
    window.location.href='./detail.html?id=' +id;
}
```

### 后端
router/product.js
```
'use strict'

let products =[
    {
        id:1,
        productName:'暴力熊',
        price:8399,
        stockNum:500,
        supplier:''
    },
    {
        id:2,
        productName:'暴力熊',
        price:8399,
        stockNum:500,
        supplier:''
    },
    {
        id:3,
        productName:'暴力熊',
        price:8399,
        stockNum:500,
        supplier:''
    },
]

let fn_list =(ctx,next)=>{
    ctx.body =products;
}
let fn_getbyId =(ctx,next)=>{
    let id =ctx.request.params.id;
    let product =products.filter(item=>{
        return item.id ==id;    // 这里采用是是大概相等，因为传入的参数是字符串
    })
    ctx.body =product[0];
}
let fn_post =(ctx,next)=>{
    
}
let fn_put =(ctx,next)=>{
    
}
let fn_delete =(ctx,next)=>{
    
}

module.exports ={
    'get /product':fn_list,
    'get /product/:id':fn_getbyId,
    'post /product':fn_post,
    'put /product/:id':fn_put,
    'delete /product/:id':fn_delete,
    'get /product/:id/comment':fn_delete,
    'get /product/:id/customer':fn_delete,
    'get /product/:id/comment/:cid':fn_delete,
    'get /product/:id/customer/:cid':fn_delete
}
```