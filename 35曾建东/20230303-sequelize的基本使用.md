# 使用步骤
下载=》配置mysql=》创建数据模型=>插入数据
## 下载
```
npm i sequelize mysql2
```
## 配置mysql
```js
//引入
const {Sequelize} = require('sequelize');
//配置
const mysql = new Sequelize('数据库名称','账号','密码',{
    dialect:'mysql',    //地址
    host:'127.0.0.1',   //方言
    timezone:'+08:00'   //时区
    pool:{      //连接池
        max:5,
        min:0,
        acquire:30000,
        idle:10000
    }
})

```

## 创建数据模型
```js
const {DataTypes} = require('sequelize')
const sequelize = requrie('mysql配置地址')
const 表名 = sequelize.define('表名',{
    //表结构
})
module.exports = 表名
```

## 插入数据
使用build+save实现插入数据库
```js
const 表名= require('模型地址')
表名.build({带数据的数据模型}).save()
```

## 在此操作遇到的问题
1. sequelize 会自动在表名后面添加s
2. sequelize 会自动添加时间戳字段('createAt','updateAt')

### 解决方案
在配置mysql时，
添加配置
```js
define:{
    timestamps:false, //时间戳，关闭
    freezeTableName:true    //冻结表名,开启
}
```