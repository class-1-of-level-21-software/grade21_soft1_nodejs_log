# promise用来做什么？

我们的需求是一次的去执行异步代码

我们的做法是在异步请求成功后的回调函数里，执行下一个异步请求

但是这样就出现了回调地狱（回调函数中嵌套了回调函数，代码的阅读性 低，维护不变，让人看着害怕）

promise就是用来解决回调地狱的

    回调地狱示例：

```js

// 需求：一次的读取a,b,c这三个文件
const fs = require("fs");

// 读a文件
fs.readFile(`${__dirname}/etc/a.txt`, "utf-8", (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
    // 读b文件
    fs.readFile(`${__dirname}/etc/b.txt`, "utf-8", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(data);
        // 读c文件
        fs.readFile(`${__dirname}/etc/c.txt`, "utf-8", (err, data) => {
          if (err) {
            console.log(err);
          } else {
            console.log(data);
          }
        });
      }
    });
  }
});

 ```

#  promise工作流程

es6的语法，es6.ruanyifeng.com

Promise对象是一个构造函数 ，用来生成promise实例

Promise构造函数接受一个函数作为参数

这个作为参数的函数，又有两个参数，这两个参数分别是resolve和reject

这两个参数它们也是函数，只不过这两个函数由 javascript 引擎提供，不用自己部署

异步操作成功后调用resolve()方法，他内部调用了then()里面的第一个参数函数

异步操作成功后调用reject()方法，他内部调用了then()里面的第二个参数函数.

```js
const fs = require("fs");
// 调用Promise构造函数，创建一个promise的实例
let p = new Promise((resolve, reject) => {
  // 写异步操作（读文件）
  fs.readFile(`${__dirname}/etc/a.txt`, "utf-8", (err, data) => {
    if (!err) {
      // 操作成功（读文件成功）
      resolve(data); // 调用resolve方法
      // 调用then()里面的第一个参数函数
    } else {
      reject(err); // 调用reject方法
      // 调用then()里面的第二个参数函数
    }
  });
});

p.then(
  (data) => {
    console.log(data);
  },
  (err) => {
    console.log(err);
  }
);

```

#  promise原理

Promise对象代表一个异步操作.

有三种状态: pending (进行中)、fulfilled (已成功)和rejected (已失败)

Promise对象的状态改变，只有两种可能:从pending变 为fulfilled和从pending变为rejected。

只有异步操作的结果，可以决定当前是哪一种状态，任何其他操作都无法改变这个状态

如果异步操作成功了(读文件成功了),从pending (进行中)变为 fulfilled (已成功) ;

如果异步操作失败了(读文件失败了),从pending (进行中)变为 rejected (已失败) ;

状态如果已经确定了， 就不会再去改变这个状态了

# promise特点及其封装

Promise新建后就会立即执行

所以不要在promise里面写其他的代码，只写这个异步操作的代码就可以了

```js
const fs = require("fs");
function getPromise(filename) {
  // 调用Promise构造函数，创建一个promise的实例
  return new Promise((resolve, reject) => {
    // 写异步操作（读文件）
    fs.readFile(`${__dirname}/etc/${filename}.txt`, "utf-8", (err, data) => {
      if (!err) {
        // 操作成功（读文件成功）
        resolve(data); // 调用resolve方法
        // 调用then()里面的第一个参数函数
      } else {
        reject(err); // 调用reject方法
        // 调用then()里面的第二个参数函数
      }
    });
  });
}

// console.log(getPromise("a"));
getPromise("a").then(
  (data) => {
    console.log(data);
  },
  (err) => {
    console.log(err);
  }
);
```
 
# promise正确写法

+ promise如何解决回调地狱

   + 链式编程解决

 + 我们用promise解决的问题：让异步操作有顺序，并且不能有回调地狱 

让异步操作有顺序本质是：

异步操作实际上是没有顺序的

在异步操作成功后的回调函数里返回另外的promise,调用他的then方法

```js
const fs = require("fs");
function getPromise(filename) {
  // 调用Promise构造函数，创建一个promise的实例
  return new Promise((resolve, reject) => {
    // 写异步操作（读文件）
    fs.readFile(`${__dirname}/etc/${filename}.txt`, "utf-8", (err, data) => {
      if (!err) {
        // 操作成功（读文件成功）
        resolve(data); // 调用resolve方法
        // 调用then()里面的第一个参数函数
      } else {
        reject(err); // 调用reject方法
        // 调用then()里面的第二个参数函数
      }
    });
  });
}

// console.log(getPromise("a"));
getPromise("a")
  .then((data) => {
    console.log(data);
    //调用函数得到一个读b文件的promise对象并返回
    return getPromise("b");
  })
  .then((data) => {
    console.log(data);
    //调用函数得到一个读c文件的promise对象并返回
    return getPromise("c");
  })
  .then((data) => {
    console.log(data);
  });
```
 
# promise的其他方法

+ catch()

        能够抓取错误的
```js
    const fs = require("fs");
    function getPromise(filename) {
      // 调用Promise构造函数，创建一个promise的实例
      return new Promise((resolve, reject) => {
        // 写异步操作（读文件）
        fs.readFile(`${__dirname}/etc/${filename}.txt`, "utf-8", (err, data) => {
          if (!err) {
            // 操作成功（读文件成功）
            resolve(data); // 调用resolve方法
            // 调用then()里面的第一个参数函数
          } else {
            reject(err); // 调用reject方法
            // 调用then()里面的第二个参数函数
          }
        });
      });
    }

    // console.log(getPromise("a"));
    getPromise("a")
      .then((data) => {
        console.log(data);
        //调用函数得到一个读b文件的promise对象并返回
        return getPromise("b");
      })
      .then((data) => {
        console.log(data);
        //调用函数得到一个读c文件的promise对象并返回
        return getPromise("c");
      })
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        console.log(err);
      });
```
 
+ all()

```js
    const fs = require("fs");
    function getPromise(filename) {
      // 调用Promise构造函数，创建一个promise的实例
      return new Promise((resolve, reject) => {
        // 写异步操作（读文件）
        fs.readFile(`${__dirname}/etc/${filename}.txt`, "utf-8", (err, data) => {
          if (!err) {
            // 操作成功（读文件成功）
            resolve(data); // 调用resolve方法
            // 调用then()里面的第一个参数函数
          } else {
            reject(err); // 调用reject方法
            // 调用then()里面的第二个参数函数
          }
        });
      });
    }

    let p1 = getPromise("a");
    let p2 = getPromise("b");
    let p3 = getPromise("c");

    // Promise.all()方法用于将多个 Promise 实例，包装成一个新的 Promise 实例
    let pAll = Promise.all([p1, p2, p3]);
    // 一个都不能少，每一个promise都要读取成功才会成功，相当于是并且
    pAll.then((data) => {
      console.log(data);
    });
```
 
+ race
```js
    const fs = require("fs");
    function getPromise(filename) {
      // 调用Promise构造函数，创建一个promise的实例
      return new Promise((resolve, reject) => {
        // 写异步操作（读文件）
        fs.readFile(`${__dirname}/etc/${filename}.txt`, "utf-8", (err, data) => {
          if (!err) {
            // 操作成功（读文件成功）
            resolve(data); // 调用resolve方法
            // 调用then()里面的第一个参数函数
          } else {
            reject(err); // 调用reject方法
            // 调用then()里面的第二个参数函数
          }
        });
      });
    }

    let p1 = getPromise("a");
    let p2 = getPromise("b");
    let p3 = getPromise("c");

    // Promise.race()方法同样是将多个 Promise 实例，包装成一个新的 Promise 实例
    let pRace = Promise.race([p1, p2, p3]);
    // 只要有一个promise执行成功，那这个pRace就成功，相当于是或者
    pRace.then((data) => {
      console.log(data);
    });
```
# 什么是async/await

 async/await是es7推出的一套关于异步的终极解决方案;主要作用就是转异步为同步。

# async/await语法格式

```js
//可写一个函数返回的是异步的promise对象,也可以定义一个异步函数；
        function fn(){
            return new Promise((resolve,reject)=>{
                   resolve(666)
            })
        }
        let p1=new Promise((n1,n2)=>{  //也可以直接写一个promise对象
                n1(999)
        })
        // 在需要使用上面异步函数的函数前面，加上async声明，声明这是一个异步函数
        async function fn2(){
        // 在异步函数前面加上await，函数执行就会等待用await声明的异步函数执行完毕之后，在往下执行
            let a1=await fn()   
            let p=await p1   
            console.log(a1,p)
        }
        fn2()
```
  
注意：async/await不能捕获错误，可以使用try/catch来进行错误的捕获

```js
        function fn() {
            return new Promise((resolve, reject) => {
                resolve(666)
            })
        }
        let p1 = new Promise((n1, n2) => {
            n1(999)
        })
        async function fn2() {
            try {
                let a1 = await fn()
                let p = await p1
                console.log(a1, p)
            } catch (err) {
                console.log(err)
            }
        }
        fn2()
```
   
# 小结：

+ 1.async函数在声明形式上和普通函数没有区别，函数声明式，函数表达式，对象方法，class方法和箭头函数等都可以声明async函数。

+ 2.任何一个await语句后面的 Promise 对象变为reject状态，那么整个async函数都会中断执行。

+ 3.async函数返回的 Promise 对象，必须等到内部所有await命令后面的 Promise 对象执行完，才会发生状态改变，除非遇到return语句或者抛出错误。也就是说，只有async函数内部的异步操作执行完，才会执行then方法指定的回调函数。
