# koa middleware

核心代码是：

```js
app.use(async (ctx, next) => {
    await next();
    ctx.response.type = 'text/html';
    ctx.response.body = '<h1>Hello, koa2!</h1>';
});
```
每收到一个http请求，koa就会调用通过app.use()注册的async函数，并传入ctx和next参数。

我们可以对ctx操作，并设置返回内容。但是为什么要调用await next()？

原因是koa把很多async函数组成一个处理链，每个async函数都可以做一些自己的事情，然后用await next()来调用下一个async函数。我们把每个async函数称为middleware，这些middleware可以组合起来，完成很多有用的功能。

middleware的顺序很重要，也就是调用app.use()的顺序决定了middleware的顺序。

此外，如果一个middleware没有调用await next()，会怎么办？答案是后续的middleware将不再执行了。这种情况也很常见.

# 处理URL

## koa-router
为了处理URL，我们需要引入koa-router这个middleware，让它负责处理URL映射。

我们把上一节的hello-koa工程复制一份，重命名为url-koa。

先在package.json中添加依赖项：
```
"koa-router": "7.0.0"
```
然后用npm install安装。

接下来，我们修改app.js，使用koa-router来处理URL：
```js
const Koa = require('koa');

// 注意require('koa-router')返回的是函数:
const router = require('koa-router')();

const app = new Koa();

// log request URL:
app.use(async (ctx, next) => {
    console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
    await next();
});

// add url-route:
router.get('/hello/:name', async (ctx, next) => {
    var name = ctx.params.name;
    ctx.response.body = `<h1>Hello, ${name}!</h1>`;
});

router.get('/', async (ctx, next) => {
    ctx.response.body = '<h1>Index</h1>';
});

// add router middleware:
app.use(router.routes());

app.listen(3000);
console.log('app started at port 3000...');
```
注意导入koa-router的语句最后的()是函数调用：
```
const router = require('koa-router')();
```
相当于：
```
const fn_router = require('koa-router');
const router = fn_router();
```
然后，我们使用router.get('/path', async fn)来注册一个GET请求。可以在请求路径中使用带变量的/hello/:name，变量可以通过ctx.params.name访问。

再运行app.js，我们就可以测试不同的URL：
```
输入首页：http://localhost:3000/
```
```
输入：http://localhost:3000/hello/koa
```