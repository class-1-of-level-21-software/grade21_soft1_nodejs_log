## 模块



#### 把函数app作为模块的输出暴露出去：

```
module.exports = app;
```

#### 引入Holle模块：

```
var app = require('./Holle');

错误:
module.js
    throw err;
          ^
Error: Cannot find module 'hello'
    at Function.Module._resolveFilename
    at Function.Module._load
    ...
    at Function.Module._load
    at Function.Module.runMain
遇到这个错误，你要检查：

模块名是否写对了；

模块文件是否存在；

相对路径是否写对了。
```



#### CommonJS规范

```
这种模块加载机制被称为CommonJS规范。在这个规范下，每个.js文件都是一个模块，
它们内部各自使用的变量名和函数名都互不冲突，
例如，hello.js和main.js都申明了全局变量var s = 'xxx'，但互不影响。


一个模块想要对外暴露变量（函数也是变量），可以用module.exports = variable;，
一个模块要引用其他模块暴露的变量，用var ref = require('module_name');
就拿到了引用模块的变量。
```

