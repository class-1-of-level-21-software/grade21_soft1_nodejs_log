#简单 INSERT 查询

```js
// 创建一个新用户
const jane = await User.create({ firstName: "Jane", lastName: "Doe" });
console.log("Jane's auto-generated ID:", jane.id);

```
#简单 SELECT 查询

```js

// 查询所有用户
const users = await User.findAll();
console.log(users.every(user => user instanceof User)); // true
console.log("All users:", JSON.stringify(users, null, 2));

```

#SELECT 查询特定属性

```js

Model.findAll({
  attributes: ['foo', 'bar']
});


```