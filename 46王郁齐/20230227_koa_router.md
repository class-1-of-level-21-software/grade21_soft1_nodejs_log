# 处理URL

koa-router，也叫koa路由（Routing）是由一个 URI（或者叫路径）和一个特定的 HTTP 方法（GET、POST 等）组成的，涉及到应用如何响应客户端对某个网站节点的访问。

每一个路由都可以有一个或者多个处理器函数，当匹配到路由时，这个/些函数将被执行。

Koa中的路由和Express有所不同，在Express中直接引入Express就可以配置路由，但是在Koa中我们需要安装对应的koa-router路由模块来实现


## 安装koa-router
```js
npm install koa-router
```

## 完整配置koa-router
引入 koa模块 以及koa-router模块 并实例化，然后配置路由启动路由

```js
var Koa=require('koa');
var Router = require('koa-router');
//实例化
var app=new Koa();
var router = new Router();

//ctx  上下文 context ，包含了request 和response等信息

//配置路由
router.get('/',async (ctx)=>{

    ctx.body='首页';
})

router.get('/news',async (ctx)=>{

    ctx.body="这是一个新闻页面"
})
/*启动路由*/
app.use(router.routes())   
    .use(router.allowedMethods());   


app.listen(3000);
```

