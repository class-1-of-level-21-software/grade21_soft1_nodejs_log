//controller文件夹下的roles.js
const fs=require('fs')
const model=require('../model');1



let url='../statics/img/IU-LILAC.jpg';



let list=[
    {
        id:1,
        name:'哈哈',
        img:url
    },{
        id:2,
        name:'嘻嘻',
        img:url
    },{
        id:3,
        name:'呵呵',
        img:url
    },{
        id:4,
        name:'嘿嘿',
        img:url
    },{
        id:5,
        name:'呐呐',
        img:url
    },
]




async function getAll(ctx,next){
    console.log(model);
    let data=await model.Role.findAll();
    console.log(data);

    ctx.render('roles.html',{list:data})
}
async function getById(ctx,next){
    let id =ctx.params.id;
    let data=await model.Role.findAll({
        where:{
            id:id
        }
    })
    ctx.render('roles.html',{list:data})

}
async function addItem(ctx,next){
    let obj=ctx.request.body;
    console.log(obj);

    await model.Role.create({
        rolename:obj.rolename
    })

    ctx.body='新增成功'
}
async function updateItem(ctx,next){

    // let id=ctx.params.id;
    // let rolename=ctx.params.rolename;
    let id=ctx.request.body.id;
    let rolename=ctx.request.body.rolename;

    console.log(id,rolename);
    await model.Role.update({rolename:rolename},{
        where:{
            id:parseInt(id)
        }
    })

    
    
}
async function delItem(ctx,next){
    let id=ctx.params.id
    console.log(id);
    await model.Role.destroy({
        where :{
            id:id
        }
    })
    ctx.body='删除成功'
}
async function findImg(ctx,next){
    ctx.type='image/jpeg';
    ctx.body=fs.readFileSync('./statics/img/IU-LILAC.jpg')
}

async function alterRoles(ctx,next){
    let id=ctx.params.id;
    let rolename=ctx.params.rolename;
    let data={
        id:id,
        rolename:rolename
    }
    
    ctx.render('alterRoles.html',{data:data})
    
}

module.exports={
    'get /roles':getAll,
    'get /roles/:id':getById,
    'get /alterRoles/:id/:rolename':alterRoles,
    'post /roles':addItem,
    'put /roles/:id':updateItem,
    'delete /roles/:id':delItem,
    'get /statics/img/IU-LILAC.jpg':findImg
}