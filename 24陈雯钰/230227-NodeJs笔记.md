## 调试
+ npm start 命令会让npm执行定义在package.json文件中的start对应命令：
```js
    "scripts":{
        "start":"node app.js"
    }
```
## koa middleware
+ 核心代码：
```js
    app.use(async (ctx,next)=>{
        await next();
        ctx.response.type = 'text/html';
        ctx.response.body = '<h1>Hello World!</h1>'
    })
```
await next()用于调用下一个中间键，调用的顺序决定了middleware的顺序

## 处理URL
## koa-router
+ 为了处理URL，可以引入koa-router这个middleware，让它负责处理URL映射
```js
const router = require('koa-router')();
const Koa = require('koa');
let app = new Koa();

router.get('/', async (ctx,next)=>{
    ctx.body = '今天很开心！';
})

app.use(router.routes());

app.listen(3000);
console.log('http://localhost:3000')
```