## NodeJs第二节课
好了，今天学习的是模块和文件配置
## 文件配置
### 文件夹名称：.vscode
### 文件名：launch.json(一定要记住launch这个单词!!!)
然后就是：
```Js
    "version":"0.2.0",
    "configurations":[
        {
            "name":"launch",
            "type":"Node",
            "program":"${workspaceRoot}\hello.js",
            "request":"launch"
        }
    ]
```
## 模块
### 模块的学习
+ 最大的好处是大大提高了代码的可维护性。
+ 使用模块还可以避免函数名和变量名冲突。
### 引入模块
```Js
// 引入hello模块:
var greet = require('./hello');

var s = 'Michael';

greet(s); // Hello, Michael!
```
今天的学习到此结束，请课后及时复习！（不信）