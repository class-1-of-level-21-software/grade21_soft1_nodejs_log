## 练习

### 1.同步方式解决返回值还没有拿到真实结果的问题

### 2.Promise

+ Promise是JS异步编程中的重要概念，异步抽象处理对象

+ Promise 是一个构造函数， new Promise 返回一个 promise对象，有两个函数类型形参resolve(正确)和reject(错误)

+ promise.then():then方法是异步执行的.catch():在链式写法中可以捕获前面then中发送的异常
```js
'use strict'

var fs = require('fs');
var http = require('http');
var path=require('path');


//如果是文件夹的操作
function fileData(fileUrl) {
    //promise是异步任务，resolve是正确时，rejects时错误时
    return new Promise((resolve, rejects) => {
        console.log('5555')
        
        //查看文件夹中是否有index.html文件
       
        if (fileUrl === './') {
            fileUrl = fileUrl + 'index.html'
        } else {
            fileUrl = fileUrl + '/index.html'
        }
        
        fs.readFile(fileUrl, 'utf-8', (err, data) => {
            if (err) {
                rejects('404')
            } else {
                resolve(data)
            }
        })
    })
}

//判断是不是文件夹
function fileStat(statUrl) {
    return new Promise((resolve, rejects) => {
        fs.stat(statUrl, (err, stats) => {
            // console.log(stats)
            //判断是不是文件夹
            if (stats.isDirectory()) {
                //是文件夹时的操作
                fileData(statUrl).then((data) => {
                    resolve(data);
                }).catch((err) => {
                    rejects(err);
                })
            } else {
                //是文件时
                fileName(statUrl).then(data=>{

                }).catch(err=>{

                })
            }
        })
    })
}

//是文件时的操作方法
function fileName(nameUrl){
    return new Promise((resolve,rejects)=>{
        
    })
}

//输出信息
var xx = http.createServer((request, response) => {
    let url = request.url;
    url = '.' + url;
    console.log(url)

    if (url !== './favicon.ico') {
        //调用判断
        fileStat(url).then(data => {
            response.end(data)
        }).catch(err => {
            response.end(err);
        })

    }

})

let post = '8080';

xx.listen(post);
console.log(`fna:http://localhost:${post}`)

```

## 三.async/await(async wait)

### asynnc

+ async是一个加在函数前的修饰符，将一个普通函数变成一个异步函数Prpmise对象。

+ 对async函数可以直接then，返回值就是then方法传入的函数。

+ 由async标记的函数称为异步函数，在异步函数中，可以用await调用另一个异步函数

### await(async wait)

+ await 也是一个修饰符，只能放在async定义的函数内。

+ await作用是等待异步函数的返回结果，同时，不阻碍其他任务的执行

+ await 修饰的如果是Promise对象：
```
1.可以获取Promise中返回的内容（resolve或reject的参数），且取到值后语句才会往下执行；

2.如果不是Promise对象：把这个非promise的东西当做await表达式的结果。

```

```js
'use strict'
async function main1(){
    return '333'
}

let res1=main1();

console.log(res1)

```
```js
'use strict'
async function main1(){
    return '222'
}


let res3=main1().then(res=>{
    console.log(res)
});

console.log(res3)
```
```js
'use strict'

//async/await(async wait)

function main2(){
    return '111'
}

async function test(){
    let x=await main2();
    console.log(x);
}


let res2=test();

console.log(res2)

```