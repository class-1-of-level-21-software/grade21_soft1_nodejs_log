# Sequelize

## 安装
    1. 使用 npm 安装sequelize
    npm i sequelize # 这将安装最新版本的 Sequelize
    2. 使用 yarn 安装sequelize
    yarn add sequelize
    3. 你还必须手动为所选数据库安装驱动程序：
    ### 使用 npm
        npm i pg pg-hstore # PostgreSQL
        npm i mysql2 # MySQL
        npm i mariadb # MariaDB
        npm i sqlite3 # SQLite
        npm i tedious # Microsoft SQL Server
        npm i ibm_db # DB2
    ### 使用 yarn
        yarn add pg pg-hstore # PostgreSQL
        yarn add mysql2 # MySQL
        yarn add mariadb # MariaDB
        yarn add sqlite3 # SQLite
        yarn add tedious # Microsoft SQL Server
        yarn add ibm_db # DB2


## 连接到数据库
    1. 要连接到数据库,必须创建一个 Sequelize 实例. 这可以通过将连接参数分别传递到 Sequelize 构造函数或通过传递一个连接 URI 来完成：
```js
    const { Sequelize } = require('sequelize');

    // 方法 1: 传递一个连接 URI
    const sequelize = new Sequelize('sqlite::memory:') //   Sqlite 示例
    const sequelize = new Sequelize('postgres://    user:pass@example.com:5432/dbname') // Postgres 示例

    // 方法 2: 分别传递参数 (sqlite)
    const sequelize = new Sequelize({
      dialect: 'sqlite',
      storage: 'path/to/database.sqlite'
    });

    // 方法 3: 分别传递参数 (其它数据库)
    const sequelize = new Sequelize('database', 'username',     'password', {
      host: 'localhost',
      dialect: /* 选择 'mysql' | 'mariadb' | 'postgres' |   'mssql' 其一 */
    });
```

## 测试链接
```js
async function test(){
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
      } catch (error) {
        console.error('Unable to connect to the database:', error);
      }
}

test()

```

## 关闭连接
1. 默认情况下,Sequelize 将保持连接打开状态,并对所有查询使用相同的连接. 如果你需要关闭连接,请调用 sequelize.close()(这是异步的并返回一个 Promise)


## 定义模型
```js
let users=sequelize.define('Users',{
    id:{
        type:DataTypes.INTEGER,
        primaryKey:true
    },
    username:DataTypes.STRING,
    age:DataTypes.INTEGER
},{
    timestamps:false
})
```
用sequelize.define()定义Model时，传入名称Users，默认的表名就是Users。第二个参数指定列名和数据类型，如果是主键，需要更详细地指定。第三个参数是额外的配置，我们传入{ timestamps: false }是为了关闭Sequelize的自动添加timestamp的功能。所有的ORM框架都有一种很不好的风气，总是自作聪明地加上所谓“自动化”的功能，但是会让人感到完全摸不着头脑。

接下来，我们就可以往数据库中塞一些数据了。我们可以用Promise的方式写：
```js
users.sync().then(()=>{
    users.create({
        id:1,
        username:'呵呵',
        age:16
    })
})
```
也可以用await写：
```js
async function xixi(){
    users.create({
    id:2,
    username:'嘻嘻',
    age:17
})
}
xixi()
```

## Promises 和 async/await

Sequelize 提供的大多数方法都是异步的,因此返回 Promises. 它们都是 Promises, 因此你可以直接使用Promise API(例如,使用 then, catch, finally).
当然,使用 async 和 await 也可以正常工作.