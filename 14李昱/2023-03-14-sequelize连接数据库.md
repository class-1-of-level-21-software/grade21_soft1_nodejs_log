#sequelize连接数据库
```js
//引入模块
const {Sequelize}=require('sequelize')

//mysql配置
const { MYSQL_PORT,
        MYSQL_PWD,
        MYSQL_USER,
        MySQL_HOST,
        MySQL_DATABASE
}=require('../config/default.config')
///创建连接
const sequelize=new Sequelize(MySQL_DATABASE,MYSQL_USER,MYSQL_PWD,{
    dialect:'mysql',
    host: MySQL_HOST,
    port:MYSQL_PORT,
    logging:false
})

//检测连接是否成功
sequelize.authenticate().then(()=>{
    console.log('OK!');
}).catch((err)=>{
    console.log('FAIL!!!'+err);
})


module.exports=sequelize
```js
