#Sequelize 实现增删改查

##1.查询列表+分页

```html

        <table id='_table'>
            <thead>
                <tr class="heart_tr">
                    <th class="heard_th">#</th>
                    <th class="heard_th">Author</th>
                    <th class="heard_th">Article</th>
                    <th class="heard_title">Update time</th>
                    <th class="heard_title">operate</th>
                </tr>
            </thead>
            {% for item in articles %}

            <tbody class="_tbody">

                <tr class="random_th">
                    <th>{{item.id}}</th>
                    <th>{{item.article_Author}}</th>
                    <th>{{item.Article_name}}</th>
                    <th>{{item.updataAt}}</th>
                    <th><a href="/article/edit?id={{item.id}} && page={{page}}">edit</a></th>
                    <th><a href="/article/delete?id={{item.id}}">del</a></th>
                </tr>


            </tbody>
            {% endfor %}

        </table>
        <div class="page">
            <ul id="btn_up"><a href="/article?page={{page - 1 }}">上一页</a></ul>
            {% for i in total %}
            <li id="btn_num"><a href="/article?page={{i}}">{{i}}</a></li>
            {% endfor %}
            <ul id="btn_down"><a href="/article?page={{page - 0 + 1 }}">下一页</a></ul>
        </div>
    </div>

```
```js
 async getarticle(ctx, next) {
          //获取前端的页数
          let page = ctx.query.page ? ctx.query.page : 1
          //数据量
          let pagesize = 12
          let id = ctx.query.id
          
          //查询     
          let where = {}
          if (id) {
               where.id = id
          }
          let data = await article.findAll({
               where,
               limit: pagesize,
               offset: (page - 1) * pagesize
          }).then(data => {

               return data
          })


          //渲染前端
          await ctx.render('article', {
               articles: data,
               page: page,

          })


     }
```
https://gitee.com/Cchen-jialun/picture/raw/master/20230319/L6WF6c%5EEm

##2.新增

```html
    <form action="/article/add/1" method="post">
        <div class="container">
            <legend>add article....</legend>
            <div>
                <input type="text" name="author" >
            </div>
            <div>
                <input type="text" name="article" >
            </div>
            <div>
                <input type="submit" value="add" >
            </div>
        </div>
    </form>
    <div>
        <input type="submit" onclick="location.href=('/article')" value="返回">
    </div>
```

```js

    //新增
     async addarticle(ctx, next) {
          await ctx.render('add')
     }
     //新增写入数据库
     async addArticleDb(ctx, next) {
          let aut = ctx.request.body.author
          let art = ctx.request.body.article
          let date = new Date()
          await article.create({
               article_Author: aut,
               Article_name: art,
               updataAt: date
          })
          //重定向
          ctx.redirect('/article');
     }

```
https://gitee.com/Cchen-jialun/picture/raw/master/20230319/h*RJ0HxoTKfH.gif

#3.删除

```html

<th><a href="/article/delete?id={{item.id}}">del</a></th>

```



```js
     //删除
     async deleteArticle(ctx, next) {
          let id = ctx.query.id
          let where = {}
          if (id) {
               where.id = id
          }
          let f = await article.destroy({
               where,
          })

          ctx.redirect('/article')
     }

```


https://gitee.com/Cchen-jialun/picture/raw/master/20230319/E*WZm733SvRc.gif

#4.编辑

```html
<form action="/article/edit/1" method="post">
        <div class="container">
            <legend>edit article....</legend>
            {% for item in articles %}
            <div>
                <input type="text" name="id" value={{item.id}} Readonly>
            </div>
            <div>
                <input type="text" name="author" value={{item.article_Author}} >
            </div>
            <div>
                <input type="text" name="article" value='{{item.Article_name}}' >
            </div>
            {% endfor %}
            <div>
                <input type="submit" value="edit" >
            </div>
        </div>
    </form>
    <div>
        <input type="submit" onclick="location.href=('/article')" value="返回">
    </div>
```

```js
//编辑
     async geteditArticle(ctx, netx) {
          let page = ctx.query.page
          let id = ctx.query.id
          let where = {}
          if (id) {
               where.id = id
          }
          let data = await article.findAll({
               where
          })
          await ctx.render('edit', {

               articles: data
          })
     }

     //编辑数据库
     async editArticle(ctx, next) {
          let page = ctx.request.body.page
          let id = ctx.request.body.id
          let aut = ctx.request.body.author
          let art = ctx.request.body.article
          let date = new Date()
          console.log(page);
          let where = {}
          if (id) {
               where.id = id
          }
          let result = await article.update({
               article_Author: aut,
               Article_name: art,
               updataAt: date
          }, {
               where
          })
          ctx.redirect('/article')
     }
}
```

https://gitee.com/Cchen-jialun/picture/raw/master/20230319/R63g!*Z6Mcf*.gif

