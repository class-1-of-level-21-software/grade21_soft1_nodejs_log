#sequelize 创建模型对象

```js
//引入数据模型
const {DataTypes}=require('sequelize')
//引入连接数据库
const seq=require('../db/db')

//创建数据库模型对象
const article=seq.define('Articles',{
    //定义类型
    article_Author:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    Article_name:{
        type:DataTypes.STRING,
        allowNull:false
    },
    updataAt:{
        type:DataTypes.DATE,
        allowNull:false
    }
},{
    tableName:'Article',
    freezeTableName:true
})
// 强制同步 //清楚数据
// article.sync({force:true})

module.exports=article

```