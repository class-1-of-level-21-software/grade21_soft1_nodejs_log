# 模块
1. 要在模块中对外输出变量，用：
```js
module.exports = variable;
```
```js
'use strict';
//先创一个hello.js文件，里面可以写方法
function add(a,b){
    return a+b;
}

//可以写数组
let arr=[1,2,3];

//可以写对象
let a={
    name:'呵呵',
    age:66
}
//在模块中对外输出变量，输出的变量可以是任意对象、函数、数组等等。
module.exports=a
```

2. 要引入其他模块输出的对象，用：
```js
var foo = require('other_module');
```
```js
//再创一个main.js文件
'use strict';
//引入其他模块输出的对象
let add=require('./hello');
//输出
console.log(add);
```
# CommonJS规范
* 这种模块加载机制被称为CommonJS规范。在这个规范下，每个.js文件都是一个模块，它们内部各自使用的变量名和函数名都互不冲突，例如，hello.js和main.js都申明了全局变量var s = 'xxx'，但互不影响。

* 一个模块想要对外暴露变量（函数也是变量），可以用module.exports = variable;，一个模块要引用其他模块暴露的变量，用var ref = require('module_name');就拿到了引用模块的变量。

