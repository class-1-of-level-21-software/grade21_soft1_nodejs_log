# 处理post请求和传参 

## 一.传参

### 问号传参:ctx.query

+ 使用ctx.query接收?号的传参数据

+ 如 http://localhost:4000/?name=ll

```js
'use strict'

//1.导入Koa

let Koa=require('koa')
let router=require('koa-router')();
//2.new一个koa实例
let app=new Koa();

router.get('/',async (ctx,next)=>{
    let par=ctx.query
    console.log(par)
    ctx.body='你好'
})

app.use(router.routes())

//4.监听运行

let port =4000;

app.listen(port)

console.log(`服务:http://localhost:${port}`)
```


### param传参:ctx.params

+ 使用ctx.params接收param传参

+ 如 http://localhost:4000/8
```js
'use strict'

//1.导入Koa

let Koa=require('koa')
let router=require('koa-router')();
//2.new一个koa实例
let app=new Koa();

router.get('/:id',async (ctx,next)=>{
    //?号传参
    // let par=ctx.query

    //params传参
    let par=ctx.param
    console.log(par)
    ctx.body='wohao'
})

app.use(router.routes())

//4.监听运行

let port =4000;

app.listen(port)

console.log(`服务:http://localhost:${port}`)
```

### 两种传参一起使用
+ 如 http://localhost:4000/8?neme=ll
```js
'use strict'

//1.导入Koa

let Koa=require('koa')
let router=require('koa-router')();
//2.new一个koa实例
let app=new Koa();

router.get('/:id',async (ctx,next)=>{
    //?号传参
    let par1=ctx.query

    //params传参
    let par=ctx.params

    console.log(par)
    console.log(par1)
    
    ctx.body='1111'
})



app.use(router.routes())

//4.监听运行

let port =4000;

app.listen(port)

console.log(`服务:http://localhost:${port}`)
```


## 二.处理post请求

### koa-bodyparser

+ koa-bodyparser 这个中间件可以将post请求的参数转为json格式返回

+ 添加依赖项:在命令终端中输入安装
```js
    yarn add koa-bodyparser/npm add koa-bodyparser

+ 引用时，require('koa-bodyparser')返回的不是函数:
```js
const bodyparser = require('koa-bodyparser');
```

+ 在合适的位置加上：
```js
//这个koa-bodyparser必须在router之前被注册到app对象上
app.use(bodyParser());
app.use(router,routes())
```

### 处理post请求

+ ctx.request.body:接收post请求传递的参数

koa.js
```js
'use strict'

//1.导入Koa

let Koa=require('koa')
let router=require('koa-router')();
let bodyParser=require('koa-bodyparser')
//2.new一个koa实例
let app=new Koa();

router.get('/:id',async (ctx,next)=>{
    //?号传参
    let par1=ctx.query

    //params传参
    let par=ctx.params

    console.log(par)
    console.log(par1)
    
    ctx.body='2222'
})

//处理post请求
router.post('/',async (ctx,next)=>{
    let data=ctx.request.body
    console.log(data)
    ctx.body=data
})


app.use(bodyParser())
app.use(router.routes())

//4.监听运行

let port =4000;

app.listen(port)

console.log(`服务:http://localhost:${port}`)
```
