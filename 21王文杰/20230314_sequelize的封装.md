# sequelize的封装

## utils文件夹（工具文件夹，放着方法）

+ modelFn.js文件

```js
'use strict'

const fs = require('fs')
const { Sequelize } = require('sequelize')
//查找文件
function fn_file(path) {
    path = path || './model'

    let files = fs.readdirSync(path)
    // console.log(files)

    return files.filter(item => {
        //过滤index.js文件和后缀不是js的文件
        return item !== 'index.js' && item.endsWith('.js')
    })
}

//设置一个对象存储对应的数据库

function fn_Url(file, db) {
    let obj = {}
    // console.log(file)
    file.forEach(item => {
        console.log(item)
        let model = require('../model/' + item)//获取对应文件暴露出来的数据库数据
        let modelName = item.replace('.js', '')//提取去除.js的文件名
        let lowmodeltName = modelName.toLowerCase()//将文件名变成小写的
        // console.log(modelName)

        obj[modelName] = db.define(lowmodeltName, model, {
            tableName: 'app_' + lowmodeltName
        })

    })

    obj.sync = async function (force) {
        if (force) {
            return db.sync({ force: true })
        }
        return db.sync()
    }
    
    console.log(obj)
    return obj

}

//暴露方法
module.exports = {
    fn_file,
    fn_Url
}
```

## model文件夹
+ index.js
```js
'use strict'

const { Sequelize } = require('sequelize')
const { fn_file, fn_Url } = require('../utils/modelFn')


//创建实例化
let db = new Sequelize('db1', 'root', 'root', {
    host: 'localhost',// 不止是可以连接本地数据库，也可以连接远程的数据库
    dialect: 'mysql'// 指定的数据库驱动，通俗来说，就是你使用的到底是哪种数据库
})


let file = fn_file();

let obj = fn_Url(file, db)
// console.log(obj)


module.exports = obj
```

+ User.js和Role.js文件内容基本一致，就放一个
```js
'use strict'

const { DataTypes } = require('sequelize')


//创建表的基本数据类型
let obj = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username: DataTypes.STRING
}

//将数据暴露出去
module.exports=obj
```

## app.js
```js
'use strict'

const model = require('./model')

model.sync(true).then(() => {
    model.User.create({
        username: '6'
    })
})
```