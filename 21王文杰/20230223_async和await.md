## 一.async/await(async wait)

### asynnc

+ async是一个加在函数前的修饰符，将一个普通函数变成一个异步函数Prpmise对象。

+ 对async函数可以直接then，返回值就是then方法传入的函数。

+ 由async标记的函数称为异步函数，在异步函数中，可以用await调用另一个异步函数

### await(async wait)

+ await 也是一个修饰符，只能放在async定义的函数内。

+ await作用是等待异步函数的返回结果，同时，不阻碍其他任务的执行

+ await 修饰的如果是Promise对象：
```
1.可以获取Promise中返回的内容（resolve或reject的参数），且取到值后语句才会往下执行；

2.如果不是Promise对象：把这个非promise的东西当做await表达式的结果。

```

```js
'use strict'
async function main1(){
    return '666'
}

let res1=main1();

console.log(res1)

```

```js
'use strict'
async function main1(){
    return '777'
}


let res3=main1().then(res=>{
    console.log(res)
});

console.log(res3)
```
```js
'use strict'


function main2(){
    return '烤鸡腿'
}

async function test(){
    let x=await main2();
    console.log(x);
}


let res2=test();

console.log(res2)

```