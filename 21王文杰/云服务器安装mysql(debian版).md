### 一、安装依赖
```js
apt install lsb-release
```

### 二、下载工具并安装软件包
```js
cd /tmp && wget https://dev.mysql.com/get/mysql-apt-config_0.8.18-1_all.deb # 如果下载慢，可以本地下载后上传到该目录
dpkg -i mysql-apt-config_0.8.18-1_all.deb
	-i	安装软件包
	-r	删除软件包
	-l	显示已安装软件包列表
	-L	显示于软件包关联的文件
	-c	显示软件包内文件列表
```
![1](./img/1.png)
![2](./img/2.png)
![3](./img/3.png)
![4](./img/4.png)
### 三、更新状态，配置并安装
```js
# 执行成功后，更新
apt update 
# W: GPG 错误：http://repo.mysql.com/apt/debian buster InRelease: 由于没有公钥，无法验证下列签名： NO_PUBKEY 467B942D3A79BD29
# E: 仓库 “http://repo.mysql.com/apt/debian buster InRelease” 没有数字签名。
# 手动执行：apt-key adv --keyserver keyserver.ubuntu.com --recv-keys  467B942D3A79BD29|报错信息中的NO_PUBKEY的值
# 安装mysql
apt install mysql-server
```
![5](./img/5.png)
![6](./img/6.png)
操作到这一步就可以了(这只是可以在你的云服务器上登录mysql,如果需要远程连接自己去查去，我也不会)
### 四、查看当前版本
```js
apt policy mysql-server
systemctl enable mysql # 开启mysql
systemctl disable mysql # 禁用mysql
systemctl restart mysql # 重启mysql
systemctl stop mysql # 关闭mysql
systemctl status mysql # 查看mysql运行状态
```

### 五、卸载
```
apt autoremove mysql-server
sudo apt-get -f install
sudo apt-get remove --purge mysql-\*
sudo apt-get install mysql-server mysql-client
```

### 六、远程
```
 show databases;
 use mysql;
 update user set host = '%' where user = 'root';
 flush privileges;
```