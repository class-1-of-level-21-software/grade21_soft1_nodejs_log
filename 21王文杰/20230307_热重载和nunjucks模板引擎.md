# 热重载和nunjucks模板引擎

## 一.热重载:nodemon

### 安装

+ 方法一:全局安装
```js
$ npm i -g nodemon

删除: npm un -g nodemon
```



## 二.nunjucks模板引擎



### 用法

### nunjucks 插值语法

+ nunjucks 的插值语法和我们平常写 VUE 的语法一样, 只需要写双大括号{{}} 即可以完成插值引用.

### 渲染:render
index.html
```js
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>halo</title>
</head>
<body>
    {{username}}666
</body>
</html>
```
users.js
```js
'use strict'

const nunjucks=require('nunjucks')

nunjucks.configure('views',{
    autoescape:true
})

async function getAll(ctx, next) {
    ctx.body=nunjucks.render('index.html',{username:"777"})
}

async function getById(ctx, next) {

}

async function addItem(ctx, next) {

}

async function updateItem(ctx, next) {

}

async function delItem(ctx, next) {

}

module.exports = {
    'get /users': getAll,
    'get /users/:id': getById,
    'post /users': addItem,//新增
    'put /users/:id': updateItem,//修改
    'delete /users/:id': delItem,
}
```

### 循环:for in 循环

+ 开始和结束语句都要在 **{%  %}**中间

+ 要一个结束循环的标志. {% endfor %}

use.html
```js
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>生活</title>
</head>
<body>
    <ul>
        {% for item in items %}

        <!-- <li>{{item.id}}</li> -->

        <li>{{item.name}}</li>

        {% endfor %}
    </ul>
</body>
</html>
```
users,js
```js
'use strict'

const nunjucks=require('nunjucks')

nunjucks.configure('views',{
    autoescape:true
})

async function getAll(ctx, next) {
    ctx.body=nunjucks.render('use.html',{items:[
        {id:1,name:"1"},
        {id:2,name:"2"},
        {id:3,name:"3"},
        {id:4,name:"4"},
    ]})
}

async function getById(ctx, next) {

}

async function addItem(ctx, next) {

}

async function updateItem(ctx, next) {

}

async function delItem(ctx, next) {

}

module.exports = {
    'get /users': getAll,
    'get /users/:id': getById,
    'post /users': addItem,//新增
    'put /users/:id': updateItem,//修改
    'delete /users/:id': delItem,
}
```


### nunjucks.configure()
```
传入 path 指定存放模板的目录，opts 可让某些功能开启或关闭，这两个变量都是可选的。path 的默认值为当前的工作目录，opts 提供以下功能：
```
+ autoescape (默认值: true) 控制输出是否被转义，查看 Autoescaping

+ throwOnUndefined (default: false) 当输出为 null 或 undefined 会抛出异常
+ trimBlocks (default: false) 自动去除 block/tag 后面的换行符
+ lstripBlocks (default: false) 自动去除 block/tag 签名的空格
+ watch (默认值: false) 当模板变化时重新加载。使用前请确保已安装可选依赖 chokidar。
+ noCache (default: false) 不使用缓存，每次都重新编译
+ web 浏览器模块的配置项
    + useCache (default: false) 是否使用缓存，否则会重新请求下载模板
    + async (default: false) 是否使用 ajax 异步下载模板
+ express 传入 express 实例初始化模板设置
+ tags: (默认值: see nunjucks syntax) 定义模板语法，查看 Customizing Syntax
