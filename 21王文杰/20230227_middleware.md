# koa middleware和处理URL

## 一.koa middleware

+ koa把很多async函数组成一个处理链，每个async函数都可以做一些自己的事情，然后用**await next()**来调用下一个async函数。我们把每个async函数称为middleware

+ 调用app.use()的顺序决定了middleware的顺序

+ 一个middleware没有调用**await next()**，后续的middleware将不再执行

### await next():调用下一个async函数
```js
'use strict'

//1.导入Koa

let Koa=require('koa')

//2.new一个koa实例
let app=new Koa();

//3.use处理请求
app.use(async(ctx,next)=>{
    await next();//调用下一个async函数
    ctx.response.type='text/html'
    ctx.body='666'
})

app.use(async(ctx,next)=>{
    console.log('nihao')
})

//4.监听运行

let port =8088;

app.listen(port)

console.log(`服务:http://localhost:${port}`)
```

## 二.处理URL

### koa-router
```
Koa-router 是 koa 的一个路由中间件，它可以将请求的URL和方法（如： GET 、 POST 、 PUT 、 DELETE 等） 匹配到对应的响应程序或页面。
```
+ 添加依赖项:在命令终端中输入安装
```js
    yarn add koa-router/npm add koa-router
```
+ 引用时，require('koa-router')返回的是函数:
```js
const router = require('koa-router')();
```

### 处理get请求

+ 用**router.get**('/path', async fn)处理的是get请求

+ 通过url访问的请求都是get请求

+ routes()属性:将这个路由放进koa

```JS
'use strict'

const router=require('koa-router')();
const koa=require('koa');

//实例化koa
let app=new koa();

router.get('/',async(ctx,next)=>{
    await next();//调用下一个async函数
    ctx.body='wohao'
    console.log('8888')
})
router.get('/',async(ctx,next)=>{
    console.log('9999')
})

//无法到下一个函数
router.get('/xx',(ctx,next)=>{
    ctx.body='烤鸡腿'
    console.log('3')
})
router.get('/xx',async(ctx,next)=>{
    ctx.body='tahao'
    console.log('1111')
})


app.use(router.routes());

let post=8080;

app.listen(post);

console.log(`网站:http://localhost:${post}`)
```
