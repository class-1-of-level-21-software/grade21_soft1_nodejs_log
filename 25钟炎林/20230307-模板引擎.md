# 模板引擎
1. 新建一个views的文件夹，来一个index.html
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>hello {{name}}</h1>
</body>
</html>
```
2. 新建一个a.js
```js
'use strict';

const Koa=require('koa');
const router=require('koa-router')();
const bodyParser=require('koa-bodyparser');
const nunjucks=require('nunjucks');

let app=new Koa();

let a=nunjucks.configure('views', { autoescape: true });
let s=a.render('index.html', { name: '666' })
console.log(s);//这里输出的是1那里index.html的整个内容

router.get('/',async (ctx,next)=>{
    ctx.body=s
})

app.use(router.routes());
app.use(bodyParser())
app.listen(8188)
console.log('http://localhost:8188');
// app.listen(8088)

```