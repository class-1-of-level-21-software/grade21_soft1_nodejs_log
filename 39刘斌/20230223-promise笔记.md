### promise代码
### Promise
Promise 表示一个异步操作的状态和结果。通过 Promise规范 来学习其实现过程，有利于我们更好地掌握和使用它。

### resolve 方法
如果 Promise 是 pending，则将状态改为 fulfilled
将 Promise 的 value 赋值为 resolve 参数
如果有 onFulfilledCallback，则将其推到微队列中（onFulfilledCallback 见下文）
### then 方法
如果 Promise 是 pending 状态，
将 then 的 onFulfilled 方法追加保存到 Promise 的 onFulfilledCallback，即规范中的 PromiseFulfillReactions 列表末尾。
将 then 的 onFulfilled 方法追加保存到 Promise 的 onRejectedCallback，即规范中的PromiseRejectReactions 列表后边。
如果 Promise 是 fulfilled 状态
把 onFulfilled 推到 microtask 微任务队列中
如果 Promise 是 rejected 状态
把 onRejected 推到 microtask 微任务队列中
~~~js
"use strict"

let p = new Promise(function (resolve,reject) {
  
    setTimeout(function () {
        resolve('111')
        console.log(p);
    },2000)
})
p.then(function (value) {
    console.log('回调成功',value);
}),function () {
    console.log('回调失败');
}
~~~