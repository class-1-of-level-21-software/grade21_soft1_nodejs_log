#### jQuery可以用cdn文件

```
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
```

单击事件之重定向（H5的button）

- H5元素

```
<input type="button" value="编辑" onclick="redirectEditRole(`{{item.id}}`)">
```

- js方法里写

```
location.href = '/roleEdit/'+id;//单击事件方法里写
```

#### 格式问题

- （后台拿前端输入的数据）应用反引号

```
     {% for item in list %}
        <tr id="{{item.id}}">
            <td>{{item.id}}</td>
            <td>{{item.role_name}}</td>
            <td>{{item.remarks}}</td>
            <td>
                <!-- {{item.id}}    `{{item.id}}` -->
                <input type="button" value="编辑" onclick="redirectEditRole(`{{item.id}}`)">
                <input type="button" value="删除" onclick="delRole(`{{item.id}}`)">
            </td>
 {% endfor %}
        </tr>
```

```
//如下路由部分不大熟
'use strict';

const fs = require('fs');

//1、找到所有的路由文件
function findControllers(path) {
    path = path || './controller/';
    //如果有指定路径则查找指定路径，如果没有指定路径则默认controller(运行时被引入到app中执行，所以其地址应该指定为./controller)
    // console.log(path);
    let files = fs.readdirSync(path);
    return files.filter(item => {
        return item !== 'index.js';
    });
}

//2、遍历注册所有的路由
function registryRouter(controllerFiles, router) {
    controllerFiles.forEach(item => {
        let tmpModule = require('../controller/' + item.replace('.js', ''));
        for (let key in tmpModule) {
            let tmpArr = key.split(' ');

            let method = tmpArr[0];
            let url = tmpArr[1];
            let fn = tmpModule[key];

            if (method === 'get') {
                router.get(url, fn);
            } else if (method === 'post') {
                router.post(url, fn);
            } else if (method === 'put') {
                router.put(url, fn);
            } else if (method === 'delete') {
                router.delete(url, fn);
            }
        }
    })

}

module.exports = {
    findControllers,
    registryRouter
}
```

