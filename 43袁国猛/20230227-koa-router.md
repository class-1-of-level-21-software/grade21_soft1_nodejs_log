### koa-router

```
添加依赖项：npm install koa-router

const Koa = require('koa');

// 注意require('koa-router')返回的是函数:

const router = require('koa-router')();//koa-router的语句最后的()是函数调用
// 创建一个Koa对象表示web app本身:
const app = new Koa();

router.get('/hello/:name', async (ctx, next) => {

  var name = ctx.params.name;

  ctx.response.body = `<h1>Hello, ${name}!</h1>`;

});

router.get('/', async (ctx, next) => {

  ctx.response.body = '<h1>Index</h1>';

});
//注册路由
app.use(router.routes());
// 在端口3000监听:
app.listen(3000);

console.log('此服务运行在：http:localhost:3000');
```

