# 处理静态文件的middleware
1. 新建一个statics文件夹里面用来放静态文件
2. 控制器，再原有的基础上加点料
```js
const fs=require('fs')

let url='../statics/img/IU-LILAC.jpg';

let list=[
    {
        id:1,
        name:'哈哈',
        img:url
    },{
        id:2,
        name:'嘻嘻',
        img:url
    },{
        id:3,
        name:'呵呵',
        img:url
    },{
        id:4,
        name:'嘿嘿',
        img:url
    },{
        id:5,
        name:'呐呐',
        img:url
    },
]

async function getAll(ctx,next){
    
    ctx.render('index.html',{list:list})
}
async function getById(ctx,next){
    
}
async function addItem(ctx,next){
    
}
async function updateItem(ctx,next){
    
}
async function delItem(ctx,next){
    
}
async function findImg(ctx,next){
    ctx.type='image/jpeg';
    ctx.body=fs.readFileSync('./statics/img/IU-LILAC.jpg')
}

module.exports={
    'get /roles':getAll,
    'get /roles/:id':getById,
    'post /roles':addItem,
    'put /roles/:id':updateItem,
    'delete /roles/:id':delItem,
    'get /statics/img/IU-LILAC.jpg':findImg
}
```
3. 新建一个处理静态文件的middleware
```js
'use strict';

const fs=require('fs');
const mime=require('mime')

function staticFiles(){
    return async (ctx,next)=>{
        let tmpPath=ctx.request.path;
        if (tmpPath.startsWith('/statics')) {
            let fullUrl= __dirname+tmpPath;
            
            if (fs.existsSync(fullUrl)) {
                ctx.type=mime.getType(fullUrl);
                ctx.body=fs.readFileSync(fullUrl);
            }else{
                ctx.body='404'
            }
        }else{
            await next()
        }
    }
}

module.exports=staticFiles;
```
4. 然后在app.js里面插入中间件就好了
```js
app.use(staticFiles())
```
