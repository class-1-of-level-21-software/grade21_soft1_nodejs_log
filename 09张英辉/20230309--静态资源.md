## 通过 Express 内置的 express.static 可以方便地托管静态文件，例如图片、CSS、JavaScript 文件等。
将静态资源文件所在的目录作为参数传递给 express.static 中间件就可以提供静态资源文件的访问了。

先安装NodeJs、npm(自带)，国外的镜像资源访问慢的话，可以考虑使用taobao的资源，用npm安装nrm来管理registry的路径,比较常用的是taobao，也可以用cnpm或者其他的

## npm初始化项目
```
npm init
```
初始化时可以对package.json进行配置，全部默认即可，若有兴趣可看下express的package.json，一般情况下标准的配置项，express的package.json中都有体现

## 安装express
express的安装很简单，使用以下命令安装
```
npm i express --save
```
编写app.js
app.js代码如下：
```
const express = require('express')
const path = require('path')
const app = express()

app.use(express.static(path.join(__dirname, 'public')))

app.listen(8080, () => {
  console.log(`App listening at port 8080`)
})
```
其中最主要的部分是app.use(express.static(path.join(__dirname, 'public')))，该行代码是在express添加中间件，设置静态资源路径为public，所有的HTML、CSS、JS等文件都放在public下即可，后续代码迁移直接将public下的代码copy到Java Web的webRoot中就行

## 添加测试页面
在public中添加测试页面
```
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Web管理平台</title>
</head>
<body>
  <h1>Web管理平台</h1>
</body>
</html>
```