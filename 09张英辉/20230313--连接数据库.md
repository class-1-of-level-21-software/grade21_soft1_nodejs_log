# 连接数据库
```
'use strict'
const {Sequelize,DataTypes}=require('sequelize')

let db=new Sequelize('db1','root','root',{
    host:'localhost',
    dialect:'mysql'
})

let User=db.define('users',{
    id:{
        type:DataTypes.INTEGER,
        Primarykey:true
    },
    username:DataTypes.STRING
});

let Role=db.define('roles',{
    id:{
        type:DataTypes.INTEGER,
        Primarykey:true,
        autoIncrement:true
    },
    roleName:DataTypes.STRING
},{tableName:'appRole'})
db.sync({force:true}).then(()=>{
    user.create({
        id:5,
        username:'user005',
    }).then(res=>{
        User.findAll().then(rows=>{
            rows.forEach(item=>{
                console.log(item.dataValues);
            })
        })
    });
    Role.create({
        roleName:'超级管理员'
    })
});
```