nodeJS模块化
模块分类：
内置模块 由Node.js官方提供的，例如 fs、path、http等

自定义模块

用户创建的js文件

第三方模块

非官方提供的内置模块，使用前需download
1，先输入代码在js上
'use strict';

console.log('Hello, world.');
2，再在控制台输入
node js文件名称.js
进入交互模式在控制台输入node
退出输入.exit
运行
创建一个vscode文件夹创建launch后输入
 {
        "varsion"："0.2.0",
        "configurations":[
            {
                "name":"Launch",
                "type":"node",
                "request":"launch",
                "program":""
            }
        ]
    }