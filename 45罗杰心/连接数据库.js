'use strict'

// 1 引入模块

const {Sequelize,DataTypes}=require('Sequelize')

// 2初始化一个实例
let db  =new Sequelize('db2','root','root',{
    dialect:'mysql',
    host:'localhost'
})
//3 定义模型
let Blog = db.define('blog',{
    id:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
        
    },
    title:DataTypes.STRING,
    content:DataTypes.STRING,
    author:DataTypes.STRING,
    cover:DataTypes.STRING

})


//4 同步模型到数据库
db.sync();
//5.CRUD