# Node.js 解析JSON
```
    键：值对是基础。

    {} 包含一个元素。

    []包含一个元素数组。

    一个元素可以有多个key ：value对。

    值可以是简单的值，例如数字或字符串等，也可以是元素或数组。

    数组中的元素可以使用索引访问

    多个键：值对或元素用逗号分隔
```

### Node.js JSON解析程序
nodejs-parse-json.js
```
// json数据
var jsonData = '{"persons":[{"name":"John","city":"New York"},{"name":"Phil","city":"Ohio"}]}'; 
 
// 解析json
var jsonParsed = JSON.parse(jsonData); 
 
// 访问元素
console.log(jsonParsed.persons[0].name);
```

运行nodejs-parse-json.js的终端输出
```
arjun@arjun-VPCEH26EN:~/workspace/nodejs$ node nodejs-parse-json.js 
John
```


### Node.js解析JSON文件
sample.json
```
 { 
    "persons": [{ 
            "name": "John", 
            "city": "Kochi", 
            "phone": { 
                "office": "040-528-1258", 
                "home": "9952685471"
            } 
 
        }, 
        { 
            "name": "Phil", 
            "city": "Varkazha", 
            "phone": { 
                "office": "040-528-8569", 
                "home": "7955555472"
            } 
        } 
    ] 
 }
```
Node.js JSON文件解析程序
nodejs-parse-json-file.js
```
// 引入文件系统模块
var fs = require('fs'); 
 
// 读取文件sample.json文件
fs.readFile('sample.json', 
    // 读取文件完成时调用的回调函数
    function(err, data) {  
        // json数据
        var jsonData = data; 
 
        // 解析json
        var jsonParsed = JSON.parse(jsonData); 
 
        // 访问元素
        console.log(jsonParsed.persons[0].name + "'s office phone number is " + jsonParsed.persons[0].phone.office); 
        console.log(jsonParsed.persons[1].name + " is from " + jsonParsed.persons[0].city); 
 });
```
运行上面的Node.js程序
运行nodejs-parse-json-file.js的终端输出
```
arjun@arjun-VPCEH26EN:~/workspace/nodejs$ node nodejs-parse-json-file.js 
John's office phone number is 040-528-1258
Phil is from Kochi



2



# Node.js 创建模块

### 创建一个Node.js模块
```
exports.<function_name> = function (argument_1, argument_2, .. argument_N) {  /** function body */ };
```

### 计算器–Node.js模块
Calculator.js
```
// 返回两个数字的加法
exports.add = function (a, b) { 
    return a+b; 
 };  
 
// 返回两个数字的差
exports.subtract = function (a, b) { 
    return a-b; 
 };  
 
// 返回两个数的乘积
exports.multiply = function (a, b) { 
    return a*b; 
 };
```

moduleExample.js
```
var calculator = require('./calculator'); 
 
var a=10, b=5; 
 
console.log("Addition : "+calculator.add(a,b)); 
console.log("Subtraction : "+calculator.subtract(a,b)); 
console.log("Multiplication : "+calculator.multiply(a,b));
```
```
$ node moduleExample.js 
Addition : 15
Subtraction : 5
Multiplication : 50
```



3



# 解析URL

### 分步指南
第1步：包括网址模块
```
var url = require(‘url‘);
```

第2步：将URL带到变量中
```
var address = ‘http://localhost:8080/index.php?type=page&action=update&id=5221‘;
```

第3步：使用解析功能解析网址
```
var q = url.parse(address,true);
```

第4步：使用点运算符提取HOST，PATHNAME和SEARCH字符串
```
q.host q.pathname q.search
```

第5步：使用查询功能解析URL搜索参数
```
var qdata = q.query;
```

第6步：访问搜索
```
qdata.type qdata.action qdata.id
```


### URL解析为Node.js中的可读部分
```
var url = require('url'); 
var address = 'http://localhost:8080/index.php?type=page&action=update&id=5221'; 
var q = url.parse(address, true); 

console.log(q.host); //返回'localhost：8080'
console.log(q.pathname); //返回'/index.php'
console.log(q.search); //returns '?type=page&action=update&id=5221'

var qdata = q.query; // 返回一个对象：{类型：页面，操作：'update'，id ='5221'}
console.log(qdata.type); //返回“页面”
console.log(qdata.action); //返回“更新”
console.log(qdata.id); //返回“ 5221”
```
### 终端输出
```
$ node urlParsingExample.js 
localhost:8080
/index.php
 ?type=page&action=update&id=5221
page
update
5221
```