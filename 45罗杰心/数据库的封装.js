const  {sequelize,DataTypes}= require('sequelize')
const fs =require('fs');
//初始化一个实例
let db = new sequelize('XXX','root','root',{
    dialect:'mysql',
    host:'localhost'
})
let XXX = db.define(
    'xxx',{
        id:{
            type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
        }
    },haha:DataTypes.STRING
)


function findModel(){
    let path = './model';
    let files = fs.readdirSync('path');
    return files.filter(item=>{
        return item.endsWith('.js') && item !=='index.js';

    });
}
let obj = {};
function defineModel(files){
    files.forEach(item=>{
        let TmpModule = require('./'+item);
        let uperFileName = item.replace('.js','');
        let lowFileName = uperFileName.toLowerCase();
        obj[uperFileName]=db.define(lowFileName,TmpModule,{
            charset:'utf8'
        })
    })
}
let files = findModel();
defineModel(files);

obj.sync=async function(force){
    if(force){
        return db.sync({force:true})
    }
    return db.sync();
}