要使用sequelize连接数据库，你需要先安装sequelize和相应的数据库驱动1。然后，你可以使用Sequelize构造函数创建一个sequelize实例，传入数据库的配置信息2。例如，如果你想连接到MySQL数据库，你可以这样写：
```js
const { Sequelize } = require('sequelize');
const sequelize = new Sequelize('database', 'username', 'password', {
  host: 'localhost',
  dialect: 'mysql'
});
```
接下来，你可以使用sequelize.define方法定义模型，并与数据库同步2。例如，如果你想定义一个User模型，你可以这样写：
```js
const User = sequelize.define('User', {
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    validate: {
      isEmail: true
    }
  }
});

// 创建表
await User.sync();
```
最后，你可以使用模型的方法来操作数据2。例如，如果你想创建一个新的用户并查询所有用户，你可以这样写：
```js
// 创建用户
const newUser = await User.create({
  name: 'Alice',
  email: 'alice@example.com'
});

// 查询所有用户
const users = await User.findAll();
```