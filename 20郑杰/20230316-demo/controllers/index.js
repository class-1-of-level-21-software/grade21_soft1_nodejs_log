'use strict';

const router = require('koa-router')();
const bodyparser = require('koa-bodyparser');
const {findControllerFiles,registerRouter}=require('../utils/tools')
////






function processRouter(app) {
    let controllerFiles = findControllerFiles();
    registerRouter(controllerFiles,router);
    app.use(bodyparser());
    app.use(router.routes())
}


module.exports=processRouter;