'use strict';

const model = require('../model')

async function fnGetAll(ctx, next) {
    ctx.body = await model.User.findAll();
};

async function fnGetById(ctx, next) {
    let id = ctx.request.params.id;
    console.log(id);
    let user = model.User.findAll({
        where:{
            id:id
        }
    })
    let res = user.length > 0 ? role[0].dataValues : {};
    console.log(res);
    ctx.body = res;
}



async function fnAdd(ctx, next) {
    let user = ctx.request.body;
    console.log(user);
    model.User.create({
        userName:user.username
    })
    ctx.body = "新增用户成功"
}

async function fnUpdate(ctx, next) {
    let id = ctx.request.params.id;
    let param = ctx.request.body;

    model.User.update({userName:param.username},{
        where:{
            id:id
        }
    });
    ctx.body = "update successfully"
}

async function fnDel(ctx, next) {
    let id = ctx.request.params.id;
    model.User.destroy({
        where:{
            id:id
        }
    })
    ctx.body="delete successfully"
}

module.exports = {
    'get /user': fnGetAll,
    'get /user/:id': fnGetById,
    'post /user': fnAdd,
    'put /user/:id': fnUpdate,
    'del /user/:id': fnDel
}