'use strict';

const Koa = require('koa');
const controllers=require('./controllers/index')

let app = new Koa();

controllers(app);

app.listen(8888);

console.log('http://localhost:8888');


