'use strict';

const {DataTypes}=require('sequelize');

let obj = {
    id:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        aotuIncrement:true
    },
    userName:{
        type:DataTypes.STRING
    }
}

module.exports = obj