'use strict';

const { Sequelize, DataTypes } = require('sequelize');

const fs = require('fs');

let db = new Sequelize('db1', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql'
})
//
function findModelFiles(path) {
    path = path || "./model";
    let ModelFiles = fs.readdirSync(path);
    return ModelFiles.filter(item => {
        return item.endsWith('.js') && item != 'index.js'
    })
}

let obj ={};

function defineModel(modelFiles) {
    modelFiles.forEach(item => {
        let tempFile = require('../model/'+item);
        let modelName=item.replace('.js','');
        let LowModelName = modelName.toLowerCase();
        obj[modelName] = db.define(LowModelName,tempFile,{
            tableName : 'app_'+LowModelName
        })
    })
}

let modelFiles = findModelFiles();
defineModel(modelFiles);

obj.sync = db.sync();

module.exports = obj;