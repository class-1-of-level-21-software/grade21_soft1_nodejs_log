# 云服务器安装Mysql

完整地安装步骤网站:https://blog.csdn.net/qq_30818545/article/details/122868975
## 一.登录自己的服务器账号
```js
ssh root@域名

密码:
```

## 二.安装命令

### 安装依赖
+ 第一条命令
```js
    apt install lsb-release
```
+ 第二条命令
```js 
    cd /tmp && wget https://dev.mysql.com/get/mysql-apt-config_0.8.18-1(版本号吧)_all.deb
```
+ 第三条命令
```js
    dpkg -i mysql-apt-config_0.8.18-1(版本号吧)_all.deb
    -i 安装软件包
    -r 删除软件包
    -l 显示已安装软件包列表
    -L 显示与软件包关联的文件
    -c 显示软件包内文件列表
```


### 上面执行完成后

+ 更新
    ```js
        apt update
    ```
    + 报错
    ```js
    E: The repository 'http://repo.mysql.com/apt/debian buster InRelease' is not signed.
    ```

    + 解决方法
    ```js
        apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 467B942D3A79BD29(这个是上面， NO_PUBKEY 467B942D3A79BD29 的值)
    ```
    + 再次输入
    ```js
        apt yodate
    ```

+ 安装mysql
    ```js
        apt install mysql-server
    ``` 
    + 输入要设置的密码，然后回车
    

+ 完成上述步骤后,再输入查看安装状态
```js
    apt policy mysql-server
```


## 三.安装完成后
```js
    systemctl status mysql //查看mysql运行状态
    systemctl enable mysql //开启mysql
    systemctl disable mysql //禁用mysql
    systemctl restart mysql //重启mysql
    systemctl stop mysql //关闭mysql
```


## 四.卸载mysql 
```js
    + apt autoremove mysql-server

    + sudo apt-get -f install

    + sudo apt-get remove --purge mysql-\*

    + sudo apt-get install mysql-server mysql-client
```
## 五.远程
```js
 + show databases;
 
 + use mysql;
 
 + update user set host = '%' where user = 'root';
 
 + flush privileges;
```
## 其他详细请见网站