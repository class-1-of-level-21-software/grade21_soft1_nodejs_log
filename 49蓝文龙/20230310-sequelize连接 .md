# 一.使用Sequelize

## 安装Sequelize
```js
使用 npm
npm i sequelize /npm add sequelize

使用 yarn
yarn add sequelize

```

## 为所选数据库安装驱动程序

+ 上课讲mysql
```js
使用 npm
npm i pg pg-hstore # PostgreSQL
npm i mysql2 # MySQL
npm i mariadb # MariaDB
npm i sqlite3 # SQLite
npm i tedious # Microsoft SQL Server
npm i ibm_db # DB2


使用 yarn
yarn add pg pg-hstore # PostgreSQL
yarn add mysql2 # MySQL
yarn add mariadb # MariaDB
yarn add sqlite3 # SQLite
yarn add tedious # Microsoft SQL Server
yarn add ibm_db # DB2
```
## 详细的
+ Sequelize的中文文档: https://www.sequelize.cn/core-concepts/getting-started

# 二.安装mysql

安装步骤的详细地址: https://blog.csdn.net/SoloVersion/article/details/123760428

+ 装5.7.x的更稳定(也不知道是不是真的)

# 三.使用

## 运行mysql

方法一:
+ 右键打开任务管理器,在服务里打开mysql

方法二:

+ win+R,打开运行框，输入services.msc直接打开服务，在里面打开mysql


## 使用命令行，查看数据库的连接

+ win+R，输入cmd

+ 输入mysql，看看能不能正常运行，如果不可以就有添加一下环境变量

+ 连接一下,输入
```
    mysql -u root -p
```
+ 输入密码，成功就连接上了

+ 创建数据库,**必须用;结束**
```
    create database 库名;
```

+ 进入数据库
```
    use 库名
```

+ 查看库中的表
```
    show tables
```

+ 查看表的内容
```
    select * from 表名
```