# Post请求：

*  在html语言中，有两种方式给服务器发送表单。一种是POST一种是GET。POST把表单打包后隐藏在后台发送给服务器；GET把表单打包发送前，附加到URL（网址）的后面。

* POST的安全性要比GET的安全性高。

## 优缺点：

* 安全，URL不会暴露我们的信息，浏览器会保存我们的上下报文，一旦发送过请求后，立即就会销毁，任何人都无法通过读你报文来窃取你的信息

* 传递的数据长！理论上POST请求可以发送无穷大的信息，所以，图片、文件的上传都是通过POST请求发送的

* POST请求不方便信息共享，如果我们需要分享一篇博客，想把url发送给朋友，如果使用post请求，参数则无法分享，因为url中无法携带任何的页面信息，仅仅只有url的实际地址

## 依赖代码：

1. 安装koa-bodyparser
```
yarn install koa-bodyparser
```

2. 导入koa-bodyparser的包
```
const parser = require('koa-bodyparser')
```

3. 调用
```
Koa的实例名.use(bodyParser());//这里的P要大写
//leware的顺序很重要，这个koa-bodyparser必须在router之前被注册到app对象上。
```

## 练习：
```
'use strict'

const Koa = require('koa');
const router = require('koa-router')()
const bodyParser = require('koa-bodyparser');



let app = new Koa();

router.get('/',async (ctx,next)=>{
    ctx.response.body='彼之砒霜'
})

router.post('/gg',async (ctx,next)=>{
    let data =ctx.request.body;
    ctx.body=data;
})

app.use(bodyParser())
app.use(router.routes())

let port = 8080;
app.listen(port)

console.log(`running is this:http://localhost:${port}`)

------------------

//创建一个index.http的响应文件来测试

POST http://localhost:8080/gg HTTP/1.1
Content-Type: application/json

{
    "username":"我是小天才",
    "password":"你是什么玩意",
    "power":"吾之蜜糖"
}
````


