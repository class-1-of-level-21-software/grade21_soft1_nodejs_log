## async
* async和await都是异步操作函数

* 同步函数：当一个函数是同步执行时，那么当该函数被调用时不会立即返回，直到该函数所要做的事情全都做完了才返回。比如说在银行排队办理业务，要等到前面一个人办完才能到下一个。

* 异步函数：如果一个异步函数被调用时，该函数会立即返回尽管该函数规定的操作任务还没有完成。比如一个人边吃饭，边看手机，边说话，就是异步处理的方式。

* 简单来说async就是声明一个function的函数异步，使这个函数返回一个Promise的对象

```nodejs
'use strict'

async function xx(){
    return 参数；
}

//返回的是：Promise{值}
```

## await

* 大概的意思就是等待的意思wait,等待一个异步函数的执行完成

* await只能放在async里使用，而不是async要await才能用
* await返回的结果是Promise执行后的结果值，或是Promise的resolve值或reject值。

* 最最重要的是await在异步函数操作中有阻塞的流程，直到结果返回之后，才能继续执行后面的代码。

```nodejs
'use strict'

async function yy(){
    let xx='404';
    xx = await yy();

    return xx;
}
//返回的是（404）

``` 

## Koa框架

* koa是由Express原班人马打造的，致力于成为一个更小、更富有表现力、更健壮的Web框架。

* koa是Express的下一代基于Node.js的web框架，目前有1.x和2.0两个版本。

1. //导入Koa
```
const Koa=require('koa')
```
2. //new一个Koa实例
```
let 名字=new Koa();
```
3. //use处理请求
```
名字.use(async (ctx,net)=>{
    回调
})
```
4. //listen监听
```
名字.listen(端口)；
```