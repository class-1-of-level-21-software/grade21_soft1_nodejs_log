# Promise大概使用的方法
* 同步编程通常来说易于调试和维护，然而，异步编程通常能获得更好的性能和更大的灵活性。异步的最大特点是无需等待。

## 语法：
```javascript
'use strict'

function 名字(params) {
    return new Promise(resolve,reject){

    }
}
```

* return返回的参数是一个promise的字符串

* 在实例promise对象里有then和catch的方法

```javascript
'use strict'

    new Promise(){

    }.then(callback回调){

    }
```

```javascript
'use strict'

    new Promise(){

    }.then(callback回调){

    }.catch(callback回调){

    }
```

## 课上的读文件的练习题用promise的方法：

