# Sequelize的封装

## 代码：
```javascript
//app.js

'use strict'

const { Sequelize,DataTypes } = require('sequelize')
const DB = require('./User')
const model = require('./model')

DB.sync({force:true}).then(()=>{
    DB.create({
       
        userName:'国王'
    })
    DB.create({
        
        userName:'侍卫'
    })
})

model.sync().then(()=>{
    model.Student.create({
        studentName:'小王同学'
    })
})
```
```javascript
//   ./model/index.js

const { Sequelize, DataTypes } = require('sequelize');


let db = new Sequelize('db1', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql' 
});

const fs = require('fs');

function findModel(path) {
    path = path || './model';
    let files = fs.readdirSync(path);
    return files.filter(item => {
        return item.endsWith('.js') && item !== 'index.js'
    });
}

let obj = {}

function defindMode(files) {
    files.forEach(item => {
        let tmpModule = require('../model/' + item);
        let modelName = item.replace('.js', '');
        let lowModelName = modelName.toLowerCase();

        obj[modelName] = db.define(lowModelName, tmpModule, {
            tableName: 'app' + lowModelName
        }
        )
    }
    )
}

let files = findModel();
defindMode(files);

obj.sync = async function () {
    return db.sync({ force: true });
}


module.exports = obj
```
```javascript
//   ./model/Student.js

const { DataTypes } = require('sequelize');


let obj = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    studentName: DataTypes.STRING
}

module.exports = obj
```

## 效果图

![](https://s1.ax1x.com/2023/03/17/ppGuh5R.png)
![](https://s1.ax1x.com/2023/03/17/ppGu5P1.png)
![](https://s1.ax1x.com/2023/03/17/ppGufa9.png)
![](https://s1.ax1x.com/2023/03/17/ppGuWVJ.png)
![](https://s1.ax1x.com/2023/03/17/ppGu2b4.png)
![](https://s1.ax1x.com/2023/03/17/ppGuI8x.png)

