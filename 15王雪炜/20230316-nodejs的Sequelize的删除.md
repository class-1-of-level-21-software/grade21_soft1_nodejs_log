# Sequelize的删除
## 代码
// view/roles

```
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="../statics/js/roles.js"></script>
    <table>
        <tr>
            <th>主键Id</th>
            <th>角色名称</th>
            <th>备注</th>
            <th>操作</th>
        </tr>
        {% for item in list %}
        <tr id="{{item.id}}">
            <td>{{item.id}}</td>
            <td>{{item.role_name}}</td>
            <td>{{item.remarks}}</td>
            <td>
                <input type="button" value="编辑" onclick="redirectEditRole(`{{item.id}}`)">
                <input type="button" value="删除" onclick="delRole(`{{item.id}}`)">
            </td>
        </tr>
        {% endfor %}
    </table>
    
```
// static/role.js
```
function delRole(id) {
    if (confirm(`你确定要删除id为${id}的角色吗？`)) {
        axios.delete('/roles/' + id).then(() => {
            let nn = $('#' + id);
            nn.remove();
        });
    }
}
```

// app.http 
```
@url=http://localhost:prot
### 删除角色
DELETE {{url}}/roles/4 HTTP/1.1
```