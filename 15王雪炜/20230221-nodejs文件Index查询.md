# isDirectory
* 查询参数文件夹的路径
* 返回的是一个Boolean的值（true,false）
* 可以无参调用isDirectory

# ReadFile

* 从本地文件路径读出一个数据到一个文件中，且支持同步和异步执行操作。

* 今天大概讲了找目录下寻找index.html文件的方法没有则显示404，有显示返回的内容。注：该题使用异步的话数据没传出来。

