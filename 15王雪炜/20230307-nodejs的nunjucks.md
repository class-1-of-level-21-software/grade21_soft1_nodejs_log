# nunjucks

## 什么是nunjucks?

* 就是一个nodejs的模块引擎

* 模块引擎：模板引擎就是基于模板配合数据构造出字符串输出的一个组件

## 安装nunjucks
```javascript
npm install nunjucks
```

## 导入nunjucks

```javascript
const nun = require('nunjucks')
```

## nunjucks的configore的属性和render方法

* configore是nunjucks的配置项

* render是对nunjucks的模块引擎的渲染有字符串渲染renderString()

