# MySQL的插入语句 define

## 代码
```javascript
'use strict'

const {Sequelize,DataTypes} = require('sequelize')

let database=new Sequelize('db1','root','root',{
    host:'127.0.0.1',
    dialect:'mysql'
})

let data = database.define('users',{
    id:{
        type:DataTypes.INTEGER,
        primaryKey:true
    },
    username:DataTypes.STRING
},{tableName:'appUsers'})

data.sync().then(()=>{
    data.create({
        id:1,
        username:'公主',
    })
})

```

## 效果图

![](https://i.imgtg.com/2023/03/14/fLvlg.png)

## 解析

1. 安装Sequelize
```
npm install sequelize
```
2. 引入sequelize
```
const {sequelize,DataTypes}= require('sequelize')
```
3. 连接MYSQL
```
//实例化一个sequelize
let database = new Sequelize('数据库名'.'用户名','密码',{
    host:服务器地址,
    dialect:数据库类型，
})
```
4. 设置数据表的属性
* define()用来定义模块，并且有返回对象，其他模块可以再调用
```
database.define('参数',{
    id:{
        primaryKey:主键
        autoIncrement:自增
        DataTypes:数据类型

    }
},{tableName:数据表名})
```

5. 写入数据库
//使用sync()同步写入
```
data.sync().then(
    data.create(
        {
            数据参数
        }
    )//创建数据表
)
```