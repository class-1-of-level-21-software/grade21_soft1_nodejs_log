# nodejs模型理解

## 什么是nodejs模型

* 在写代码到后期越来越多越长繁杂时，就会导致后期十分难维护，为了编写代码，可以把函数，对象，数组放在不同的文件下在nodejs的环境下称为模块(module).

## module的例子

```nodejs
'use strict'

let add = function(x,y){
    return x+y;
};

module.export = add;

//模块引用

'use strict'

let res = require('./文件的相对路径');

let s = res(3,9);

console.log(s);
//s=12
```

1. module.export是载体载入模块中
2. require('./文件的相对路径')是引用已有的模块调用
3. 引用时文件地址不写全写模块名时会，Node会依次在内置模块、全局模块和当前模块下查找文件，会获得一个报错
4. 代码写入时要遵循CommonJS的规范，作用是内部各自使用的变量名和函数名都互不冲突

## nodejs的程序使用

```nodejs
'use strict'

console.log("hello world!");

//此时我们要打开终端快捷键ctrl+shift+`

运行：

    node 文件名.
    node 文件
    node 文件的相对路径

结果：

    hello world！


```

* node的交互模式
1. node-interactive-env
1. 简单来说就是在进行交互模式时，我们输入的js的代码可以直接在终端执行.