# nodejs的Koa-router

## 定义：
* 我们在访问不同路径地址寻址时，都会回应网页内容代用不动相对应的函数，处理URL的middleware要一个个接对应的处理函数。

## Koa-router的代码主体：

1. 引入Koa-router的包
2. 实例化一个Koa-router的对象化
3. 声明多个及以上的Koa-router的get函数
4. 在列式调用多个get函数
5. 监听端口

## 实例
```nodejs
'use strict'

const Koa = require('koa');
const router = require('koa-router')();

let app = new Koa();

router.get('/',async (ctx,next)=>{
    ctx.body='QianTian诺手1v3三杀'
})

router.get('/ss',async (ctx,next)=>{
    ctx.body='3个打1个被反杀，你们会不会玩'
})

app.use(router.routes());

let port = 8080;

app.listen(port);

console.log(`服务器执行于：http://localhost:${port}`)
···