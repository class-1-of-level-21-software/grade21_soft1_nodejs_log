# node.js第一课笔记

1. 什么是nodejs

* Node.js发布于2009年5月，由Ryan Dahl开发，是一个基于Chrome V8引擎的JavaScript运行环境，使用了一个事件驱动、非阻塞式I/O模型,让JavaScript 运行在服务端的开发平台，它让JavaScript成为与PHP、Python、Perl、Ruby等服务端语言平起平坐的脚本语言。

* Node.js 就是运行在服务端的 JavaScript。
* Node.js 是一个基于Chrome JavaScript 运行时建立的一个平台。
* Node.js是一个事件驱动I/O服务端JavaScript环境，基于Google的V8引擎，V8引擎执行Javascript的速度非常快，性能非常好。


2. nodejs的npm

* npm（即 node package manager ）是Node的包管理工具

```ndoe
//nodejs的版本检查
npm -v

//nodejs升级
npm -g

//本地安装
npm install express

//全局安装
npm install express -g
```

