### 模块

在Node中，require一个文件实际上是在require这个文件定义的模块。所有的模块都拥有一个

　　对隐式module对象的引用，当你调用require时实际上返回的是没module.exports属性。对于

　　module.exports的引用同样也能写成exports。

　　在每一个模块的第一行都隐式的包含了一行下面的代码：


```
const exports=module.exports={}

```

注意：如果你想要导出一个函数，你需要将这个函数赋值给module.exports。将一个函数赋值给exports将会为

exports引用重新赋值，但是module.exports依然会指向原始的空对象。

因此我们可以像这样来定义一个function.js模块来导出一个对象:


```
module.exports=function(){
    return{name:'jane'}
}
```


然而在另一个文件中require这个模块：

`const fund=require('./function')`

require的一个重要行为就是它缓存了module.exports的值并且在未来再次调用require时返回同样的值。