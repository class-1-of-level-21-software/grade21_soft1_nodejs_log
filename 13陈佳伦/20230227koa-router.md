### Koa中间件使用之koa-router
### 基本配置
### 标题创建Koa应用

下面的代码创建了一个koa web服务，监听了3000端口，如果访问 http://localhost:3000/ 将返回 Not Found ，这是因为代码没有对请求做任何响应。后面将使用 koa-router 在这个基础上进行修改，使其支持不同的路由匹配。


```js
// app.js

const Koa = require('koa'); // 引入koa

const app = new Koa(); // 创建koa应用

// 启动服务监听本地3000端口
app.listen(3000, () => {
    console.log('应用已经启动，http://localhost:3000');
})
```
### 安装koa-router

`$ npm install koa-router --save`
### 使用koa-router

首先，使用 require() 引入 koa-router ，并且对其实例化（支持传递参数），然后使用获取到的路由实例 router 设置一个路径，将 '/' 匹配到相应逻辑，返回一段HTML 。接着还需要分别调用 router.routes() 和 router.allowedMethods() 来得到两个中间件，并且调用 app.use() 使用这两个中间件：


```js
const Koa = require('koa'); // 引入koa
const Router = require('koa-router'); // 引入koa-router

const app = new Koa(); // 创建koa应用
const router = new Router(); // 创建路由，支持传递参数

// 指定一个url匹配
router.get('/', async (ctx) => {
    ctx.type = 'html';
    ctx.body = '<h1>hello world!</h1>';
})

// 调用router.routes()来组装匹配好的路由，返回一个合并好的中间件
// 调用router.allowedMethods()获得一个中间件，当发送了不符合的请求时，会返回 `405 Method Not Allowed` 或 `501 Not Implemented`
app.use(router.routes());
app.use(router.allowedMethods({ 
    // throw: true, // 抛出错误，代替设置响应头状态
    // notImplemented: () => '不支持当前请求所需要的功能',
    // methodNotAllowed: () => '不支持的请求方式'
}));

// 启动服务监听本地3000端口
app.listen(3000, () => {
    console.log('应用已经启动，http://localhost:3000');
})
```

