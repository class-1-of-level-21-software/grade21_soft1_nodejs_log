#一、引入sequelize模块
```js
var Sequelize = require('sequelize');

二、连接数据库
var sequelize = new Sequelize(
    'sample', // 数据库名
    'root',   // 用户名
    'psw',   // 用户密码
    {
        'dialect': 'mysql',  // 数据库使用mysql
        'host': 'localhost', // 数据库服务器ip
        'port': 3306,        // 数据库服务器端口
        'define': {
            // 字段以下划线（_）来分割（默认是驼峰命名风格）
            'underscored': true
        }
    }
);
```
#三、定义表

相应字段有: 
type 字段数据类型(sequlize. …) 
allowNull(是否允许为空true,false) 
autoIncrement(自增, true ,false) 
unique(唯一性, true,false, string) 
comment (解释 说明)

primaryKey (对主键的设置, true,false) 
defaultValue(默认值的设置) 
field

 
```js
var User = sequelize.define(
 
    'user',
 
    {
 
        userId: {
            field: 'user_id',
            primaryKey: true,
            type: Sequelize.BIGINT,
            allowNull: false
        },
        userName: {
            field: 'user_name',
            type: Sequelize.STRING,
            allowNull: false
        },
        userIcon: {
            field: 'user_icon',
            type: Sequelize.STRING,
            allowNull: true
        },
        title: {
            field: 'title',
            type: Sequelize.STRING,
            allowNull: true
        },
        gender: {
            field: 'gender',
            type: Sequelize.ENUM('MALE','FEMALE'),
            allowNull: true
        },
        birth: {
            field: 'birth',
            type: Sequelize.STRING,
            allowNull: true
        },
        mail: {
            field: 'mail',
            type: Sequelize.STRING,
            allowNull: true
        },
        tel: {
            field: 'tel',
            type: Sequelize.STRING,
            allowNull: true
        },
        mobile: {
            field: 'mobile',
            type: Sequelize.STRING,
            allowNull: true
        },
        updateTime: {
            field: 'update_time',
            type: Sequelize.STRING,
            allowNull: true
        }
    },
    {
        tableName: 'user',
        timestamps: false,
        freezeTableName: true
    }
 
 
);
```

#四、往表里添加数据
```js
User.create({
    userId: 23,
    userName: '老杨',
    updateTime: '2016-01-22 18:37:22'
});
```