
# promise
- 等待异步函数的返回结果，同时不阻挡其他任务
```js
const fs=require('fs')
function readFile(){
	return new Promise((resolve,reject)=>{
	fs.readFile('./index.html',(err,data)=>{
		if(err){
			resolve('404')
		}
		else{
		resolve('data')
		}
	})
	})
} 
async test()=>{
	let resString = '404'
	resString await readFile()
	
}

test()
```
# async await
异步终极操作
1. await 必须放在async函数内部
2. 简化promise,替代then(),减少回调函数
3. 简洁代码,便于理解,提高开发效率
```js
async funcation func(){
let p1=await readFile(file)
}
```