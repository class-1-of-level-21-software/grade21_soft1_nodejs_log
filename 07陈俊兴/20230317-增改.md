# Sequelize的增改

## 代码

// view/roles

```
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="../statics/js/roles.js"></script>
    <table>
        <tr>
            <th>主键Id</th>
            <th>角色名称</th>
            <th>备注</th>
            <th>操作</th>
        </tr>
        {% for item in list %}
        <tr id="{{item.id}}">
            <td>{{item.id}}</td>
            <td>{{item.role_name}}</td>
            <td>{{item.remarks}}</td>
            <td>
                <input type="button" value="编辑" onclick="redirectEditRole(`{{item.id}}`)">
                <input type="button" value="删除" onclick="delRole(`{{item.id}}`)">
            </td>
        </tr>
        {% endfor %}
    </table>
    
```

// controller/user.js

```
'use strict';

async function getAll(ctx, next) {
    
}

async function getById(ctx, next) {

}
async function addItem(ctx, next) {

}
async function updateItem(ctx, next) {

}
async function delItem(ctx, next) {

}

module.exports = {
    'get /users': getAll,
    'get /users/:id': getById,
    'post /users': addItem,
    'put /users/:id': updateItem,
    'delete /users/:id': delItem,
}
```

// app.http

```
@url=http://localhost:prot

POST {{url}}/roles HTTP/1.1
Content-Type: application/json

{
    "rolename":"添加"
}

PUT {{url}}/roles/4 HTTP/1.1
Content-Type: application/json

{
    "rolename":"修改"
}
```
