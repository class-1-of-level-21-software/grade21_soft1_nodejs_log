---
一 .模型定义
---

在 **Sequelize** 中可以用两种等效的方式定义模型：

调用 **`sequelize.define(modelName, attributes, options)`**
扩展 **Model** 并调用 **`init(attributes, options)`**
定义模型后,可通过其模型名称在 **sequelize.models** 中使用该模型

为了学习一个示例,我们将考虑创建一个代表用户的模型,该模型具有一个 *`firstName`* 和一个 *`lastName`*. 我们希望将模型称为 **User**,并将其表示的表在数据库中称为 **Users**

+ 定义该模型的两种方法如下所示. 定义后,我们可以使用 **`sequelize.models.User`** 访问模型

    > 使用 *`sequelize.define`*:

    ```js
    const { Sequelize, DataTypes } = require('sequelize');
    const sequelize = new Sequelize('sqlite::memory:');

    const User = sequelize.define('User', {
    // 在这里定义模型属性
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastName: {
        type: DataTypes.STRING
        // allowNull 默认为 true
    }
    }, {
    // 这是其他模型参数
    });

    // `sequelize.define` 会返回模型
    console.log(User === sequelize.models.User); // true
    ```

### 扩展 **Model**

```js
const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:');

class User extends Model {}

User.init({
  // 在这里定义模型属性
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING
    // allowNull 默认为 true
  }
}, {
  // 这是其他模型参数
  sequelize, // 我们需要传递连接实例
  modelName: 'User' // 我们需要选择模型名称
});

// 定义的模型是类本身
console.log(User === sequelize.models.User); // true
```

> 在内部,*`sequelize.define`* 调用 *`Model.init`*,因此两种方法本质上是等效的

二 .添加数据
---

我们创建好模型之后，也可以在里面添加数据，让我们的模型变得更有意义，也可以通过输出来检验我们添加的数据是否成功

+ 我们创建模型User之后,创建实例在里面添加数据值
    > 实例代码：

    ```js
    User.sync({force:true}).then(()=>{
        User.create({
            id:1,
            username:'admin',
            password:'111',
            avatar:''
        })
    });
    ```
