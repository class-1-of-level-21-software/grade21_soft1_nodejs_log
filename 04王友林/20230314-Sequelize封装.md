# 封装
```js
'use strict';

const {Sequelize,DataTypes}=require('sequelize');
const fs=require('fs');

let db=new Sequelize('db4','root','root',{
    host:'localhost',
    dialect:'mysql'
});

function findModel(path) {
    path= path || './model';
    let ff=fs.readdirSync(path);
    return ff.filter(item=>{
        return item.endsWith('.js') && item !== 'index.js';  //排除index.js
    })
    
}

let obj={}

function defindModel(ff){  //这里的ff引用上面的
     ff.forEach(item => {
        let tmpModuel=require('../model/'+item);  // 引入model文件
        let modelName=item.replace('.js',''); 
        let lowModelName=modelName.toLowerCase();  

        obj[modelName]=db.define(lowModelName,tmpModuel,{ 
            tableName : 'app'+ lowModelName  
        })
     });
}


let ff= findModel(); //使用方法
defindModel(ff); //使用方法

obj.sync=async function(){
   return db.sync({force:true});
}


module.exports=obj
```