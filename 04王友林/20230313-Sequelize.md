# Sequelize
```js
'use strict';

const {Sequelize,DataTypes}=require('sequelize');//引入Sequelize和DataTypes模块

let db=new Sequelize('db1','root','root',{ //创建数据库账号和密码
    host:'localhost',
    dialect:'mysql'
});  

//创表
let User=db.define('users',{
    id:{
        type:DataTypes.INTEGER, 
        primaryKey:true,  //主键
        
    },
    username:DataTypes.STRING
},{tableName:'appUser'});


let Role=db.define('roles',{
    id:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true //自增
    },
    username:DataTypes.STRING
},{tableName:'appRole'}); //表的名字




//用Role的话超级管理员就不能自增了，用User的话会自增，使用db的话就只会显示一条，不会自增
db.sync({force:true}).then(()=>{  //force:true这个加上的话表示只能显示一条内容
    User.create({
        id:4,
        username:'user04'
    }).then(res=>{
        User.findAll().then(rows=>{
            rows.forEach(item=>{
                console.log(item.dataValues);
            })
        })
    });
    Role.create({
        username:'超级管理员'
    })
});
```