# 修改和删除
```js
'use strict';
const fs=require('fs');
const {Role} =require('../model');
let imgUrl= '../statics/img/imgs01.jpg';

// 希望给所有的ctx挂载上一个方法，这个方法和env.render效果一致
async function getAll(ctx,next) {
    let data=ctx.body=await Role.findAll(); //找到model里的Role的表的数据
    ctx.response.type='text/html'; //显示文本格式
    ctx.render('roles.html',{list:data});  //使用模板引擎，这样可以让roles.html里面引用Role的东西.
};

async function getById(ctx,next){
    //拿到id
    let id=ctx.request.parmas.id; //获得路径的id
    console.log(id);
    let role=await Role.findAll({  //找到指定id的数据
        where:{
            id:id
        }    
    })
    let res=role.length>0 ? role[0].dataValues :{} //findAll里收到一群对象，里面有一个dataValues的对象里面有表的数据，获取第一个值
    console.log(res);
    ctx.render('roles.html',{list:data});
     // console.log(ctx.kaixin);
       
};

async function addItem(ctx,next){
    let obj= ctx.request.body;  //获取aip.http里的post里的"rolename":"GIF"
    console.log(obj);
    // Role来自于模型文件的名称，同时也是模型名称
    Role.create({
        role_name:obj.rolename
    })

    ctx.body='新增角色成功';
}

async function updateItem(ctx,next){
    //拿到id
    let id=ctx.request.parmas.id;
    let parma=ctx.request.body;
    Role.update({role_name:parma.rolename},{
        where :{
            id:id
        }
    });
    ctx.body='更新成功';
}


async function delItem(ctx,next){
   
    let id=ctx.request.parmas.id;
    Role.destroy({
        where:{
            id
        }
    })
    ctx.body='删除成功';
}


async function roleEdit(ctx,next){
    let id=ctx.request.parmas.id;
    let role=await Role.findAll({
        where:{
            id:id
        }
    })
    let res=role.length>0 ? role[0].dataValues: {}
    ctx.render('addRole.html',{res});
}


module.exports={
    'get /roles':getAll,
    'get /roleEdit/:id':roleEdit,
    'get /roles/:id':getById,
    'post /roles':addItem,
    'put /roles/:id':updateItem,
    'delete /roles/:id':delItem
}
```

```html
<!-- addRole.html -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>修改角色</title>
</head>
<body>
    <table>
        <tr>
            <td>角色名称</td>
            <td><input type="text" name="rolename" value="{{res.role_name}}"></td>
        </tr>
        <tr>
            <td><input type="button" value="提交保存" onclick="save()"></td>
            <td><input type="button" value="返回" onclick="cancle()"></td>
        </tr>
    </table>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="../statics/js/roles.js"></script>

</body>
</html>
```

```html
<!-- roles.html -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>角色</title>
</head>
<body>
    <table>
        <tr>
            <th>主键id</th>
            <th>角色名称</th>
            <th>备注</th>
            <th>操作</th>
        </tr>
            {% for item in list %}
            <tr id="{{item.id}}">
                <td>{{item.id}}</td>
                <td>{{item.role_name}}</td>
                <td>{{item.remarks}}</td>
                <td>
                    <input type="button" value="编辑" onclick="redirectEditRole(`{{item.id}}`)">
                    <input type="button" value="删除" onclick="delRole(`{{item.id}}`)">
                </td>
            </tr>

            {% endfor %}
    </table>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="../statics/js/roles.js"></script>
</body>
</html>
```
