# async
* 申明一个function是异步的；
```js
async function test(){
    console.log('111');
    return 'jjj';
}

let x =test();

console.log(x);
```
# await
* 等待异步执行完成
* `await`即等待，用于等待一个`Promise`对象。它只能在异步函数 `async function`中使用，否则会报错
* 它的返回值不是`Promise`对象而是`Promise`对象处理之后的结果

# async/await 使用规则：
* async 表示这是一个async函数， await只能用在async函数里面，不能单独使用
* async 返回的是一个Promise对象，await就是等待这个promise的返回结果后，再继续执行
* await 等待的是一个Promise对象，后面必须跟一个Promise对象，但是不必写then()，直接就可以得到返回值

# 与Promise对比

##### 1、不再需要多层.then()方法

假设一个业务分很多步骤完成，并且每个步骤都是异步，依赖上一个步骤的结果。

##### 2、可以对Promise进行并行处理