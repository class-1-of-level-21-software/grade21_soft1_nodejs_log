## 封装：

```js
const { Sequelize, DataTypes } = require('sequelize');



// 初始化一个Sequelize的实例

let db = new Sequelize('db1', 'root', 'root', {

  host: 'localhost',// 不止是可以连接本地数据库，也可以连接远程的数据库

  dialect: 'mysql' // 指定的数据库驱动，通俗来说，就是你使用的到底是哪种数据库

});



const fs = require('fs');



function findModel(path) {

  path = path || './model';

  let files = fs.readdirSync(path);

  return files.filter(item => {

​    return item.endsWith('.js') && item !== 'index.js'

  });

}



let obj = {}



function defindMode(files) {

  files.forEach(item => {

​    let tmpModule = require('../model/' + item);

​    let modelName = item.replace('.js', '');

​    let lowModelName = modelName.toLowerCase();



​    obj[modelName] = db.define(lowModelName, tmpModule, {

​      tableName: 'app' + lowModelName

​    }

​    )

  }

  )

}



let files = findModel();

defindMode(files);



obj.sync = async function () {

  return db.sync({ force: true });

}





module.exports = obj
```

