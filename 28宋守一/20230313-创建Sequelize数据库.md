## win+R打开cmd，启动服务

`net start mysql57`

## 转到MySQL

[![ppG0pA1.png](https://s1.ax1x.com/2023/03/17/ppG0pA1.png)](https://imgse.com/i/ppG0pA1)

## 安装

[![ppG0i9K.png](https://s1.ax1x.com/2023/03/17/ppG0i9K.png)](https://imgse.com/i/ppG0i9K)

## 代码

[![ppG0F1O.png](https://s1.ax1x.com/2023/03/17/ppG0F1O.png)](https://imgse.com/i/ppG0F1O)

## 打印结果[![ppG0Ch6.png](https://s1.ax1x.com/2023/03/17/ppG0Ch6.png)](https://imgse.com/i/ppG0Ch6)

[![ppG09tx.png](https://s1.ax1x.com/2023/03/17/ppG09tx.png)](https://imgse.com/i/ppG09tx)