# 易错点

1. 引入的sequelize是类，所以引入和实例化时要将S大写，还有引入的DataTypes是复数要加s。

例如

```javaScript
    const {Sequelize,DataTypes} = require("sequelize");
```

2. 在定义模型时，设置主键primaryKey时Key的K要大写，

例如

```javaScript
    {primaryKey:true}
```

# ORM(用sequelize 链接 MySQL)

1. 引入Sequelize 和 DataTypes 
```javaScript
    const {Sequelize,DataTypes} = require("sequelize")
```

2. 实例化一个Sequelize对象并传达参数
```javaScript
    const db = new Sequelize("db","root","root",{
        host:"localhost",
        dialect:"mysql"//dialect 方言
    })
```

3. 定义一个User模型

```javaScript
    const User = db.define("user"，{
        id:{
            type:DataTypes.INTEGER,
            prumaryKey:true
        }
        username:DataTypes.STRING,
        password:DataTypes.STRING
    })
```

4. 同步并插入数据

```javaScript
    User.sync({force:true}).then(()=>{
        User.create({
            id:1,
            username:"admin",
            password:"12345"
        })
    })
```

# 命令提示符窗口操作

1. win健+R 输入services.msc,找到MySQL并启动

2. 首先配置MySQL的环境变量。

3. MySQL -u root - p 

4. 输入密码，无误则成功

5. create database db 创建db 数据库

6. use db 进入db 数据库

7. show tables 查看db数据库中得表

8. select * from users 查看表的信息
