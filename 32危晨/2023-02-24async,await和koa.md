Koa
可以用npm命令直接安装koa。先打开命令提示符，务必把当前目录切换到hello-koa这个目录，然后执行命令：

npm install koa
Koa 是一个中间件框架，可以采用两种不同的方法来实现中间件：

async function
common function
Koa 提供了一个 Request 对象作为 Context 的 request 属性。 Koa的 Request 对象提供了用于处理 http 请求的方法，该请求委托给 node http 模块的 IncomingMessage。 Koa提供了一个 Response 对象作为 Context 的 response 属性。 Koa的 Response 对象提供了用于处理 http 响应的方法，该响应委托给 ServerResponse。

Koa 对 Node 的请求和响应对象进行委托而不是扩展它们。这种模式提供了更清晰的接口，并减少了不同中间件与 Node 本身之间的冲突，并为流处理提供了更好的支持。 IncomingMessage 仍然可以作为 Context 上的 req 属性被直接访问，并且ServerResponse也可以作为Context 上的 res 属性被直接访问。