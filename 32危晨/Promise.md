Promise 的基本用法
（1）使用new实例化一个Promise对象，Promise的构造函数中传递一个参数。这个参数是一个函数，该函数用于处理异步任务。

（2）并且传入两个参数：resolve和reject，分别表示异步执行成功后的回调函数和异步执行失败后的回调函数；

（3）通过 promise.then() 处理返回结果。这里的 p 指的是 Promise实例。

then 方法
所有的 promise 对象实例里都有一个 then 方法，它是用来跟这个 promise 进行交互的。首先，then 方法会缺省调用 resolve() 函数:

catch 方法
catch 当一个 promise 被拒绝(reject)时,catch 方法会被执行：