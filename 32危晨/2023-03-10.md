nodejs Sequelize连接数据库
安装
Sequelize的使用可以通过 npm (或 yarn).

# 使用 npm
npm install sequelize # 这将安装最新版本的 Sequelize

# 使用 yarn
yarn add sequelize

数据库安装驱动程序
还必须手动为所选数据库安装驱动程序：

# 使用 npm

```javascript
npm install pg pg-hstore 	# PostgreSQL
npm install mysql2 		# MySQL
npm install mariadb 	# MariaDB
npm install sqlite3 	# SQLite
npm install tedious 	# Microsoft SQL Server
npm install ibm_db 		# DB2
```
# 使用 yarn

```javascript
yarn add pg pg-hstore 	# PostgreSQL
yarn add mysql2 	# MySQL
yarn add mariadb 	# MariaDB
yarn add sqlite3 	# SQLite
yarn add tedious 	# Microsoft SQL Server
yarn add ibm_db 	# DB2
```
连接到数据库
要连接到数据库,必须创建一个 Sequelize 实例. 这可以通过将连接参数分别传递到 Sequelize 构造函数或通过传递一个连接 URI 来完成：
```javascript

const { Sequelize } = require('sequelize');

// 方法 1: 传递一个连接 URI
const sequelize = new Sequelize('sqlite::memory:') // Sqlite 示例
const sequelize = new Sequelize('postgres://user:pass@example.com:5432/dbname') // Postgres 示例

// 方法 2: 分别传递参数 (sqlite)
const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: 'path/to/database.sqlite'
});

// 方法 3: 分别传递参数 (其它数据库)
const sequelize = new Sequelize('database', 'username', 'password', {
  host: 'localhost',
  dialect: /* 选择 'mysql' | 'mariadb' | 'postgres' | 'mssql' 其一 */
});
测试连接
可以使用 .authenticate() 函数测试连接是否正常：

try {
  await sequelize.authenticate();
  console.log('Connection has been established successfully.');
} catch (error) {
  console.error('Unable to connect to the database:', error);
}