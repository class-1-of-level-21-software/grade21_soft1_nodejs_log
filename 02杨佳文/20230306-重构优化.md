# 重构优化

简单来说就是新建一个工具文件，将可以复用的工具类代码放进去，看起来非常舒适

## 代码：

### app.js
```js
'use strict'
const koa=require('koa');
const controllers=require('./controllers')

let app=new koa();

controllers(app);

let port=5000
app.listen(port)
console.log(`kk:http://localhost:${port}`);
```

### tools.js

```js
'use strict'
const fs=require('fs');

//1.找到所有路由
function findAllRouter(path) {
    path=path||'./controllers';

    let files=fs.readdirSync(path)
    return files.filter(
        item=>{
            return item!=='index.js'
        }
    )
    
}

//遍历并注册路由

function resign(obj,router) {
    for (const key in obj) {
        let sp=key.split(' ');
        let rou=sp[0];
        let url=sp[1];
        let ff=obj[key];
        if(rou=='get'){
            router.get(url,ff)
        }else if(rou=='post'){
            router.post(url,ff)
        }else if(rou=='put'){
            router.put(url,ff)
        }
        else if(rou=='delete'){
            router.delete(url,ff)
        }
    }
        
    ;
}


module.exports={
    findAllRouter,resign
}

```


### index.js
```js
'use strict'

const router=require('koa-router')();
const bodyparser=require('koa-bodyparser');
const {findAllRouter,resign}=require('../utils/tools')

function process(app) {
    let files=findAllRouter();
    files.forEach(
        item=>{
            let cons=require('../controllers/'+item.replace('.js',''))
            resign(cons,router)
        }
    )
    app.use(router.routes());
    app.use(bodyparser())
}

module.exports=process

```

### 还是各种数据.js

重复太多只放一个

```js
'use strict'

function getAll(ctx,next) {
    ctx.body='1111';
}
function getById(ctx,next) {
    ctx.body='2222';
}
function insertItem(ctx,next) {
    
}
function updateById(ctx,next) {
    
}
function deleteById(ctx,next) {
    
}


module.exports={
    'get /student':getAll,
    'get /student/:id':getById,
    'post /student':insertItem,
    'put /student/:id':updateById,
    'delete /student/:id':deleteById

}

```
