# 前后端分离完善

## 安装的模块:package.json文件
```js
{
  "dependencies": {
    "koa": "^2.14.1",
    "koa-bodyparser": "^4.4.0",
    "koa-router": "^12.0.0",
    "koa-static": "^5.0.0",
    "koa2-cors": "^2.0.6",
    "mysql2": "^3.2.0",
    "sequelize": "^6.30.0"
  }
}

```

## controllers文件夹

### roles.js
```js
'use strict'

//引入数据库模块
const {Roles,Op}=require('../model')

//查找所有信息
async function getAll(ctx,next){
    let keyword=ctx.request.query.keyword;
    console.log('这是keyword：'+keyword)
    let res={}
    if(keyword){
        res=await Roles.findAll({
            where:{
                [Op.or]:[
                    {
                        role_name:{
                            [Op.substring]:keyword
                        }
                    },{
                        role_site:{
                            [Op.substring]:keyword
                        }
                    }
                ]
            }
        });
    }else{
        res=await Roles.findAll();
    }
    ctx.body=res
}

//查找指定id的信息
async function getById(ctx,next){
    let id=ctx.request.params.id;
    let data=await Roles.findByPk(id)

    ctx.body={
        code:1000,
        data:data,
        msg:`查询指定内容成功`
    }
}

//新增
async function addItem(ctx,next){
    let obj=ctx.request.body;
   let res= await Roles.create({
        role_name:obj.rolename,
        role_site:obj.rolesite
    })

    ctx.body={
        code:1000,
        data:null,
        msg:'新增成功'
    }
}

//修改
async function updateItem(ctx,next){
    let id=ctx.request.params.id;
    //获取修改值
    let obj=ctx.request.body;
    await Roles.update({role_name:obj.rolename,role_site:obj.rolesite},{
        where:{
            id:id
        }
    })
    ctx.body={
        code:1000,
        data:null,
        msg:'修改成功'
    }
}

//删除
async function deleteItem(ctx,next){
    let id=ctx.request.params.id;
    await Roles.destroy({
        where:{
            id:id
        }
    })
    ctx.body={
        code:1000,
        data:null,
        msg:'删除成功'
    }
}

module.exports={
    'get /roles':getAll,
    'get /roles/:id':getById,
    'post /roles':addItem,
    'put /roles/:id':updateItem,
    'delete /roles/:id':deleteItem
}
```

## model文件夹

### index.js
```js
'usr strict'

const { Sequelize,Op } = require('sequelize')

let db = new Sequelize('db1', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql'
})

const { fn_files } = require('../utils/tools')

let file = fn_files('./model')

//定义一个对象变量存储
let obj = {}

file.forEach(item => {
    let modelObj = require('../model/' + item)//接收暴露的数据类型
    let name = item.replace('.js', '')//去后缀
    let lowName = name.toLowerCase();//将字符变为小写的

    obj[name] = db.define(lowName, modelObj, {
        tableName: `app_` + lowName
    })
})

obj.Op=Op

obj.sync=async (force)=>{
    if(force){
        return db.sync({force:true})
    }
    return db.sync()
}

module.exports=obj

```
## app.js
```js
'use strict'

//引用模块
const Koa=require('koa')
//实例化
let app=new Koa()

//测试数据库,测试成功
// const model=require('./model')
// model.sync(true).then(()=>{
//     model.Roles.create({
//         role_name:'鸡腿',
//         role_site:'炸鸡店'
//     })
// })

//解决跨域问题,index页面才能显示数据库内的值
const cors=require('koa2-cors')
app.use(cors())

//注册路由
let fn_controller=require('./controllers')
app.use(fn_controller(app))


//监听端口
let post=3030;
app.listen(post)
console.log(`http://localhost:${post}`)
```

## api.http
```js
@url=http://localhost:3030
### 查看
GET {{url}}/roles HTTP/1.1

### 指定id
GET {{url}}/roles/4 HTTP/1.1

### 增加
POST {{url}}/roles HTTP/1.1
Content-Type: application/json

{
    "rolename":"yjw",
    "rolesite":"yjw"
}

### 修改
PUT {{url}}/roles/4 HTTP/1.1
Content-Type: application/json

{
    "rolesite":"yjw"
}

### 删除
DELETE {{url}}/roles/4 HTTP/1.1
```

## statics文件夹中的js文件

### role.js
```js
'use strict'
$(function () {
    //使用页面方法
    getData();
    console.log('yjw')
})

//创建一个页面方法
function getData(keyword) {
    let url = 'http://localhost:3030/roles'
    if (keyword) {
        url = url + '?keyword=' + keyword
    }
    //进入页面的时候删除一次
    let tr = $('[id^=role]')
    tr.remove()

    $.ajax({
        type: 'get',//请求方式
        url: url,//请求路径
        success: function (res) {
            res.forEach(item => {
                let html = `
                    <tr id="role${item.id}">
                        <td>${item.id}</td>
                        <td>${item.role_name}</td>
                        <td>${item.role_site}</td>
                        <td>
                            <input type="button" value="编辑" onclick="butPut(${item.id})">
                            <input type="button" value="删除" onclick="butDel(${item.id})">
                        </td>
                    </tr>`

                let tbRole = $('#tbRole')
                //添加到html页面中
                tbRole.append(html)
            })
        }
    })
}
//查找
function butFilter() {
    let keyword = $('[name=keyword]').val();
    getData(keyword)
}
//新增
function butPost() {
    location.href = 'addOrEdit.html';
}

//编辑
function butPut(id) {
    location.href = 'addOrEdit.html?id=' + id;
}

//删除
function butDel(id) {
    if (confirm(`你确定要删除id为${id}吗?`)) {
        $.ajax({
            type: 'delete',
            url: 'http://localhost:3030/roles/' + id,
            success: function (res) {
                $('#role' + id).remove();
            }
        })
    }
}
```

### roleEdit.js
```js
'use strict'
//跳转时页面的初加载
$(function () {
    //获取id
    let id = getQueryString('id')
    if (id) {
        $.ajax({
            type: 'get',
            url: 'http://localhost:3030/roles/' + id,
            success: function (res) {
                $('[name=rolename]').val(res.data.role_name);
                $('[name=rolesite]').val(res.data.role_site)
            }
        })
    }
})

function getQueryString(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    let r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return decodeURIComponent(r[2]);
    };
    return null;
}
//保存
function butSave() {
    let id = getQueryString('id');
    let rolename = $('[name=rolename]').val();
    let rolesite = $('[name=rolesite]').val();

    if (id) {
        $.ajax({
            type: 'put',
            url: 'http://localhost:3030/roles/' + id,
            data: { role_name: rolename, role_site: rolesite },
            success: function (res) {

            }
        })
    } else {
        $.ajax({
            type: 'post',
            url: 'http://localhost:3030/roles',
            data: { role_name: rolename, role_site: rolesite },
            success: function (res) {

            }
        })
    }
    location.href = 'index.html'
}


//取消并返回
function butCanel() {
    location.href = 'index.html'
}
```

## index.html
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>首页</title>
    <link rel="stylesheet" href="./statics/css/base.css">
</head>
<body>
    <div id="bar"> 
        <input type="text" placeholder="请输入关键字查找" name="keyword">
        <input type="button" value="查询" onclick="butFilter()">
        <input type="button" value="新增" onclick="butPost()">
    </div>
    <div>
        <table id="tbRole">
        <tr>
            <th>编号</th>
            <th>名称</th>
            <th>地址</th>
            <th>操作</th>
        </tr>
    </table>
    </div>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="./statics/js/role.js"></script>
</body>
</html>
```

## addOrEdit.html
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>新增或修改</title>
</head>
<body>
    <table>
        <tr>
            <td>名称</td>
            <td><input type="text" name="rolename"></td>
        </tr>
        <tr>
            <td>地址</td>
            <td><input type="text" name="rolesite"></td>
        </tr>

        <tr>
            <td><input type="button" value="提交" onclick="butSave()"></td>
            <td><input type="button" value="返回" onclick="butCanel()"></td>
        </tr>
    </table>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="./statics/js/roleEdit.js"></script>
</body>
</html>
```