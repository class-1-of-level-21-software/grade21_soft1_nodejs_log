### 安装koa的包

在命令终端中输入
```js
    方法一: yarn add koa(建议)
    
    方法二:npm install koa(地址在国外很慢，要改地址)
```
+ npm会把koa2以及koa2依赖的所有包全部安装到当前目录的node_modules目录下。

+ 目录的node_modules目录是要被忽略的

1. 添加一个忽略文件夹 **.gitignore**

2. 在文件夹中输入**node_modules**目录名


### koa的运用

+ koa将调用我们传入的异步函数来处理每一个http请求

+ 参数ctx是由koa传入的封装了request和response的变量，我们可以通过它访问request和response

+ next是koa传入的将要处理的下一个异步函数。
```js
'use strict'

//1.导入Koa

let Koa=require('koa')

//2.new一个koa实例
let app=new Koa();

//3.use处理请求
app.use(async(ctx,next)=>{
    await next();
    ctx.response.type='text/html'
    ctx.response.boby='<p>你好<p>'
})

//4.监听运行

let port =4000;

app.listen(port)

console.log(`http://localhost:${port}`)
```