# await next()

中间件的传播如果没有await next(),那么中间件将无法传播

await next()之后这个中间件将入栈

执行完中间件的注册流程之后,中间件await next()之后的代码将会继续执行

代码: 

```js
'use strict'

const koa=require('koa');
const route=require('koa-router')();
let app=new koa();
app.use(async(ctx,next)=>{
     
        await next();
        ctx.response.type='text/html'
        ctx.response.body='hello'
    })

    app.use(async(ctx,next)=>{
        console.log('1');
    })

    app.use(async(ctx,next)=>{
        console.log('2');
    })
app.listen(8080);

console.log(`kk:http://localhost:8080`);
```

效果图：

如图，代码其中一个中间件输出了2，但因为它的上一个中间件没有await next(),所以它不执行

![awaitnext](./img/awaitnext.png)


# koa-router

为了处理URL，我们需要引入koa-router这个middleware，让它负责处理URL映射。

对不同的URL调用不同的处理函数，返回不同的结果

## 安装koa-router

终端命令：npm i koa-router --save

## 试一下

代码：

```js
const koa=require('koa');
const router=require('koa-router')();
let app=new koa();

router.get('/',async(ctx,next)=>{
    ctx.response.type='text/html'
    ctx.response.body='111'
})

router.get('/abc',async(ctx,next)=>{
    ctx.response.type='text/html'
    ctx.response.body='222'
})


app.use(router.routes())

app.listen(8080)

console.log(`kk:http://localhost:8080`);

```
效果图：

根据输入的URL呈现响应的内容

![route1](./img/route1.png)

![route2](./img/route2.png)






 
