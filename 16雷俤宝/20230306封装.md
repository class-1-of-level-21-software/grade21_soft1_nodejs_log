## 封装

文件app.js
```js
'use strict'
const Koa = require('koa');
const controllers = require('./controller')

let app = new Koa();
controllers(app)

let port = 8088;
app.listen(port);
console.log(`http://localhost:${port}`);
```

index.js
```js
'use strict'


const router = require('koa-router')();
const bodyparser = require('koa-bodyparser');
const {findControllers,registryRouter} = require('../utils/tools')

//处理路由
function processRouter(app) {

    //1、找到所有路由文件
    let controllers = findControllers()
    //2、遍历注册所有路由

    registryRouter(controllers,router)

    app.use(bodyparser())
    app.use(router.routes());

}
module.exports = processRouter
```

tools.js
```js
'use strict'

const fs = require('fs')

//1、找到所有路由文件
function findControllers(path) {
    path = path || './controller/';
    console.log(path);
    let files = fs.readdirSync(path)
    // console.log(files);
    return files.filter(item => {
        return item !== 'index.js'
    })
}

//2、遍历注册所有路由
function registryRouter(controllerFiles,router) {
    controllerFiles.forEach(item => {
        let tmpModule = require('../controller/' + item.replace('.js', ''));
        console.log(tmpModule);
        for (let key in tmpModule) {
            let tmpArr = key.split(' ');

            let method = tmpArr[0];
            let url = tmpArr[1];
            let fn = tmpModule[key];

            if (method === 'get') {
                router.get(url,fn)
            } else if (method === 'post') {
                router.post(url,fn)
            } else if (method === 'put') {
                router.put(url,fn)
            } else if (method === 'delete') {
                router.delete(url,fn)
            }

        }
    })
}

module.exports = {
    findControllers,
    registryRouter
}
```

users.js
```js
'use strict'


async function getAll(ctx, next) {
    ctx.body = '这里是getAll'
}

async function getById(ctx, next) {
    ctx.body = '这里是getById'
}

async function addItem(ctx, next) {
    ctx.body = '这里是addItem'
}

async function updateItem(ctx, next) {

}

async function delItem(ctx, next) {

}

module.exports = {
    'get /users':getAll,
    'get /users/:id':getById,
    'post /users':addItem,
    'put /users/:id':updateItem,
    'delete /users/:id':delItem
}
```