## 文件服务器
设定一个目录，让Web成为一个文件服务器。解析request.url的路径，在本地找到对应文件再将其发送出去
```js
const http = require('http');
const fs = require('fs');

let x = http.createServer((req,res)=>{
    let url  = '.'+req.url;
    if (url==='./') {
        url='./index.html'
    }
    fs.readFile(url,'utf-8',(err,data) => {
        if (err) {
            res.end('404');
        }else{
            res.end(data)
        }
    })
})
let wwj=8989;

x.listen(wwj)
console.log(`http://localhost:${wwj}`);
```