## 项目重构
在一个Node.js的web应用项目里，URL的处理可以说是非常繁琐的，所以要去 进行包装重构，这样能让代码更加简洁，逻辑更加清晰明了 ，创建一个文件夹去将这些不同页面访问URL处理进行封装成模块，只用去引用
### 设置 Middleware
将一些在controllers目录和route要引入的模块封装好在一个模块中，这样去减少主程序js文件中的繁琐代码

最后把扫描controllers目录和创建router的代码从 入口js文件中提取出来，作为一个简单的middleware使用 ，命名为index.js存放在controllers目录：

这样一来，我们在主程序的js文件代码就又简化了：

```js
// 导入controller middleware:
const controller = require('./controller');
...
// 使用middleware:
app.use(controller());
...
经过重新整理后的项目具备非常好的模块化，所有处理URL的函数按功能组存放在controllers目录，今后我们也只需要不断往这个目录下加东西就可以了，主程序的js文件保持不变