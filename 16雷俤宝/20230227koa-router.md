# 基本使用
- 下载 
- 创建 
- 添加路由
## 下载
```
npm add koa-router
```

## 创建单独的路由文件 例:UserRouter.js
```js
//引入路由模块
const router = require('koa-router');

//创建路由
const myrouter = router({prefix:'/user'/*访问前缀*/})

//添加路由
myrouter.post('/',(ctx,next)=>{
    //TODO:添加用户的代码
})

//TODO:添加更多的路由

//暴露路由
module.exports = myrouter;
```
服务器中的代码：  
```js
const koa = require('koa');
const app = new koa();
const PORT = 8080;
//引入路由
const userRouter = require('路由模块路径')

//添加路由
app.use(userRouter.routes())

app.listen(PORT,()=>{
    console.log(`server running at http://127.0.0.1:${PORT}`)
})
```

