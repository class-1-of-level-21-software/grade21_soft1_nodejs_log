# 调试文件代码和模块

## 一.调试文件代码

+ 1.创建 **.vscode**文件夹

+ 2.在文件夹中创建 **launch.json**文件

+ 3.在文件中输入以下代码进行配置:
```json
{
    "version":"0.2.0",
    "configurations":[
        {
            "type":"node",
            "name":"Launch",
            "request":"launch",
            "program":"${workspaceFolder}\\main.js"
        }
    ]
}
```

## 二.模块

### 1.什么是模块
```
每一个模块都可以理解为一个独立的功能（最底层其实就是函数）。而node中的模块是以 js文件为单位 ，简单的说就是每一个js文件就是一个独立的模块，模块与模块之间都是独立的。
```

### 2.优点

+ 提高了代码的可维护性

+ 可以引用其他模块，不用重新编写代码

+ 可以避免函数名和变量名的冲突。相同名字的函数和变量完全可以分别存在不同的模块中

### 3.CommonjS规范
```
Commonjs 就是我们Node中模块化的规范。

CommonJS 规范的提出，主要是为了弥补当前 JavaScript 没有模块化标准的缺陷 。
```

## 三.输出变量
```js
    //输出的变量可以是任意对象、函数、数组等等
    在模块中对外输出变量用:module.exports=XXX;
```
## 四.引入变量
```js
    引入其他模块的输出对象用:var 变量名 = require('XXX');
```

### 代码
输出页面:he.js

```js
'use strict'

console.log("Hello Welcome!")

var add=Math.random();

function greet(name){
    console.log(add+','+name+'!');
}
//把函数greet作为模块的输出暴露出去，这样其他模块就可以使用greet函数了。
module.exports=greet;
```

引入页面:main.js
```js
'use strict'

//引用模板
var greet=require('./he');

var s='随机数';

greet(s);
```

