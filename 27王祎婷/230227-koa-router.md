## koa-router
-  Koa-router 是 koa 的一个路由中间件，它可以将请求的URL和方法（如： GET 、 POST 、 PUT 、 DELETE 等） 匹配到对应的响应程序或页面。
```js
'use strict'
//1.导入Koa
let Koa=require('koa');
// 注意require('koa-router')返回的是函数:
let router=require('koa-router')();
//2.new一个Koa实例
let app=new Koa();
//3.add router
//get请求(通过url访问的请求都是get)
router.get('/',async(ctx,next)=>{
    ctx.body='我是wyt';

})
router.get('/abc',async(ctx,next)=>{
    ctx.body='我是abc';
})
//add router middleware(将这个路由放入Koa)
app.use(router.routes());//routes是属性(将这个路由放进koa)

//4.监听运行
let port=8899;
app.listen(port);
console.log(`你的服务运行于：http://localhost:${port}`);
```
![](./img/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_272.png)

![](./img/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_271.png)

