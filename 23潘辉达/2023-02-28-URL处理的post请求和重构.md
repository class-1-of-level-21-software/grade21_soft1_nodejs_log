**Node.js**——**URL处理的post请求和重构**
===
在用**Node.js创建了web应用服务器**后，会有一些对 *URL的处理*，有**get、post、delete、put**等一类的请求，跟路由都有关系，但在一个比较完善的系统结构中，这种路由请求会很多，所以用重构，将他们去分类统一放在各自的文件夹中，这样更便于去写处理的代码，只用调用出来就好了，也可以让逻辑关系更加清晰

一 .URL处理的post请求:
---

用``router.get('/path', async fn)``处理的是**get请求**,如果要**处理post请求**，可以用``router.post('/path', async fn)``

+ 用**post请求处理URL**时，我们会遇到一个问题：*post请求通常会发送一个表单，或者JSON* ，它作为**request的body**发送，但无论是**Node.js**提供的***原始request对象，还是koa提供的request对象，都不提供解析request的body的功能***

    + 所以，我们又需要引入另一个 *middleware* 来解析原始**request请求**，然后，把解析后的参数，绑定到`ctx.request.body`中

+ **koa-bodyparser**就是用来干这个活的

    > 我们在**package.json**中添加依赖项：
    ```json
    "koa-bodyparser": "3.2.0"
    ```
    > 然后使用`npm install`安装

    > 下面修改js文件，引入koa-bodyparser：
    ```js
        const bodyParser = require('koa-bodyparser');
    // 在合适的位置加上：
        app.use(bodyParser());
    ```
    + 由于 *middleware* 的顺序很重要，这个**koa-bodyparser**必须在**router**之前被 *注册到app对象上*

> 现在我们就可以**处理post请求**了。写一个简单的登录表单：
```js
    router.get('/', async (ctx, next) => {
        ctx.response.body = `<h1>Index</h1>
            <form action="/signin" method="post">
                <p>Name: <input name="name" value="koa"></p>
                <p>Password: <input name="password" type="password"></p>
                <p><input type="submit" value="Submit"></p>
            </form>`;
    });

    router.post('/signin', async (ctx, next) => {
        var
            name = ctx.request.body.name || '',
            password = ctx.request.body.password || '';
        console.log(`signin with name: ${name}, password: ${password}`);
        if (name === 'koa' && password === '12345') {
            ctx.response.body = `<h1>Welcome, ${name}!</h1>`;
        } else {
            ctx.response.body = `<h1>Login failed!</h1>
            <p><a href="/">Try again</a></p>`;
        }
    });
```
+ 注意到我们用`var name = ctx.request.body.name || ''`拿到表单的**name字段**，如果该字段不存在，默认值设置为''

    > 类似的，**put、delete、head请求**也可以由**router处理**。

二 .重构项目
---
现在，我们已经可以处理不同的URL了，但是看看js文件，总觉得还是有点不对劲

+ 所有的URL处理函数都放到js文件里显得很乱，而且，每加一个URL，就需要修改js文件,随着URL越来越多，js文件就会越来越长

    >如果能把URL处理函数集中到某个js文件，或者某几个js文件中就好了，然后让app.js自动导入所有处理URL的函数。这样，代码一分离，逻辑就显得清楚了，最好是这样：
    ```
    url2-koa/
    |
    +- .vscode/
    |  |
    |  +- launch.json <-- VSCode 配置文件
    |
    +- controllers/
    |  |
    |  +- login.js <-- 处理login相关URL
    |  |
    |  +- users.js <-- 处理用户管理相关URL
    |
    +- app.js <-- 使用koa的js
    |
    +- package.json <-- 项目描述文件
    |
    +- node_modules/ <-- npm安装的所有依赖包
    ```

    > 我们先在controllers目录下编写index.js：
    ```js
    var fn_index = async (ctx, next) => {
        ctx.response.body = `<h1>Index</h1>
            <form action="/signin" method="post">
                <p>Name: <input name="name" value="koa"></p>
                <p>Password: <input name="password" type="password"></p>
                <p><input type="submit" value="Submit"></p>
            </form>`;
    };

    var fn_signin = async (ctx, next) => {
        var
            name = ctx.request.body.name || '',
            password = ctx.request.body.password || '';
        console.log(`signin with name: ${name}, password: ${password}`);
        if (name === 'koa' && password === '12345') {
            ctx.response.body = `<h1>Welcome, ${name}!</h1>`;
        } else {
            ctx.response.body = `<h1>Login failed!</h1>
            <p><a href="/">Try again</a></p>`;
        }
    };

    module.exports = {
        'GET /': fn_index,
        'POST /signin': fn_signin
    };
    ```
    > 这个index.js通过module.exports把两个URL处理函数暴露出来。

    > 类似的，hello.js把一个URL处理函数暴露出来：
    ```js
    var fn_hello = async (ctx, next) => {
        var name = ctx.params.name;
        ctx.response.body = `<h1>Hello, ${name}!</h1>`;
    };

    module.exports = {
        'GET /hello/:name': fn_hello
    };
    ```