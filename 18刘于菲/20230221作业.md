- - 练习

    ```
    const http=require('http');
    const fs=require('fs');
    
    const server=http.createServer((req,res)=>{
        let url=req.url;
        console.log(url);
        if (fs.existsSync('.'+url)) {
            if (fs.existsSync('.'+url+'/index.html')) {
                url='.'+url+'/index.html'
            }else if(fs.existsSync('.'+url)){
                url='.'+url
            }
        }else if(fs.existsSync('.'+url+'.html')){
            url='.'+url+'.html'
        }else if(fs.existsSync('.'+url+'.txt')){
            url='.'+url+'.txt'
        }else if(fs.existsSync('.'+url+'.md')){
            url='.'+url+'.md'
        }else if(url==='/'){
            url='./index.html'
        }
    
        fs.readFile(url,'utf-8',(err,data)=>{
            if (err) {
                res.end('404')
            }else{
                res.end(data)
            }
        })
    })
    
    server.listen(8088);
    
    console.log('1111');
    ```