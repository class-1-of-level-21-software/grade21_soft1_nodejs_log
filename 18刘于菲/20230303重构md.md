1.介绍
node.js是一个基于Chrome V8引擎的JaveScript运行环境，简单来说就是运行在服务端的JavaScript,是一个事件驱动、非阻塞式的I/O模型，速度快，性能好。前端的蓬勃发展除了各大框架的流行之外，还有因为node.js的兴起，这使得前台和后台的数据交互更加便捷更加轻松。由此也产生了一个新的概念——全栈工程师，一个全栈工程师可以完成前端页面架构还可以完美实现后台数据的交互，熟练掌握node.js已经成为每位前端工作者基本技能。

2.使用
与node.js打包下载的还有一个叫NPM的安装包管理工具，能解决node.js代码部署上很多问题。我们可以利用npm工具使用第三方包或命令行程序，也可以上传自己编写的包或命令行程序供别人使用。npm安装第三方包的过程也十分简便，windows系统用cmd打开命令行，输入npm install +（包的名称，例如webpack）就行。

3.模块化
3.1模块的输入与输出
Node.js 的模块分为两类，一类为原生（核心）模块，一类为文件模块。原生模块在 Node.js 源代码编译的时候编译进了二进制执行文件，加载的速度最快。另一类文件模块是动态加载的，加载速度比原生模块慢。但是 Node.js 对原生模块和文件模块都进行了缓存，于是在第二次 require 时，是不会有重复开销的。其中原生模块都被定义在 lib 这个目录下面，文件模块则不定性。

文件模块可以是JavaScript代码文件（.js作为文件后缀）、也可以是JSON格式文本文件（.json作为文件后缀）、还可以是编辑过的C/C++文件（.node作为文件后缀）。文件模块输入方式通过require('/文件名.后缀')，require('./文件名.后缀')，requrie('../文件名.后缀') ，文件后缀可以省略；以"/"开头是以绝对路径去加载，以"./"开头和以"../"开头表示以相对路径加载，而以"./"开头表示同级目录下文件。文件后缀可以省略，Nodejs尝试加载的优先级 js文件 > json文件 > node文件，下面给出nodeJS模块的加载顺序：



实际应用中我们经常使用.js文件作为一个模块进行输入输出，输出通过exprots或者module.exports，其中exports是一种简写形式，等于在文件开头有这样一行命令：var exports = module.exports，浏览器在编译的时候也会把它当成module.exports指令，注意我们在使用exports时不能对它进行赋值等操作，否则会切断与module.exports之间的联系，导致输出错误。

3.2 nodeJS重构路由功能
上面的内容主要介绍了nodeJs的作用、nodeJs的基本语法以及它的遵循规范，下面我们开始一个demo，实现路由功能：

//server.js
var http = require('http') //输入node.js核心模块

function startServer(route){
    var onRequest = function(req,res) {
        route(req.url) //传入请求的路径
    }
    var server = http.createServer(onRequest)
server.listen(3000)  //监听一个端口
}

module.exports.startServer = startServer;  //输出模块
我们新建一个server.js模块，在这个模块中，我们定义了一个startServer函数，这个函数监听了一个3000端口，函数执行的时候调用route方法。我们先将这个模块输出

//index.js
var server = require("./server");  //输入./server模块
var router = require('./router');  //输入./router模块
var handler = require("./handler")  //输入./handler模块
var handle = {};   
handle["/"] = handler.home;           
handle["/home"] = handler.home;
handle["/list"] = handler.list;
handle["/add"] = handler.add
server.startServer(router.route,handle)
route()接收到了请求路径，我们希望它接收到不同的路径会调用不同的方法，因此我们可以先把要执行的方法作为属性值传给一个handle对象，它的键就等于route接收到的请求路径。于是我们分别把创建route.js和handler.js，在index.js中把请求路径和handle对象传给route函数

  //server.js
var http = require('http') //引入node.js核心模块

function startServer(route,handle){
var onRequest = function(req,res) {
route(req.url,handle,res) //传入请求的路径
}
var server = http.createServer(onRequest)
server.listen(3000) //监听一个端口
}
module.exports.startServer = startServer; //输出模块


//route.js
function route(pathname,handle，res){
  if(typeof handle[pathname] == "function"){
     handle[pathname](res)
  }else{
     res.end("404:connot find anything") //未匹配到路径的时候，响应“404”页面
  }
}
module.exports.route = route;


//handler.js
function home (res){
    res.end("home")
}
function list (res){
    res.end("list")
}
function add(res){
   res.end("add")
}
module.exports = {
    home:home,
    list: list,
    add:add
}
