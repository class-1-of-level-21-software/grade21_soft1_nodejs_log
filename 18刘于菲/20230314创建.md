

1、打开cmd命令窗口，安装sequelize-auto

```
npm install -g sequelize-auto
```

在使用sequelize-auto之前需要安装全局的mysql（举例mysql）

```
npm install -g mysql
```

2、数据库新建表

然后运行sequelize-auto -o "./mysqltest" -d db -h localhost -u root -p 3306 -x root -t user -e mysql

在mysqltest目录下就会生成user.js(user表对应的model)

3、将user.js复制到node项目下即可使用