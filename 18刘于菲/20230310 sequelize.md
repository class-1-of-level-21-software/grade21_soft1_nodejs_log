## 安装 sequelize

在项目里面安装 sequelize 和 mysql ，也可以使用 sequelize-cli ，这里只是先学习下 sequelize 的
`npm i sequelize`
`npm i mysql`

## 连接数据库

首先在新建一个 config.js ，用来存放数据库相关的配置

```java
const config = {
    database:'test',
    username:'www',
    password:'www',
    host:'localhost',
    port:3306
}

module.exports = config
```

然后新建一个 db.js

```verilog
const config = require('./config')

// connect db
const sequelize = new Sequelize(config.database, config.username, config.password, {
  host: config.host,
  port: config.port,
  dialect: 'mysql',
  operatorsAliases: false
})
module.exports = sequelize
```

添加下面的代码可以检查是否连接成功：

```javascript
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  })
```

## 新建表，同步表

创建一个公共的方法，用来新建 model

```javascript
/**
 * 定义数据模型的公共方法
 * @param {any} name 模型名称【数据库表名】
 * @param {any} attributes 数据字段集合
 * @returns 数据模型对象
 */
function defineModel (name, attributes) {
    var attrs = {}

    for (let key in attributes) {
        let value = attributes[key]
        if (typeof value === 'object' && value['type']) {
            value.allowNull = value.allowNull || false
            attrs[key] = value
        } else {
            attrs[key] = {
                type: value,
                allowNull: false
            }
        }
    }

    // 附加公共字段
    attrs.createAt = {
        type: Sequelize.BIGINT,
        allowNull: false
    }
    attrs.updateAt = {
        type: Sequelize.BIGINT,
        allowNull: false
    }
    // 状态：0表示有效，1表示无效，2表示已删除，默认为0.
    attrs.status = {
        type: Sequelize.INTEGER,
        allowNull: false
    }
    // 版本
    // attrs.status = {
    //     type: Sequelize.INTEGER,
    //     allowNull: false
    // }

    // 调用seq的方法定义模型并返回
    return sequelize.define(name, attrs, {
        tableName: name,
        timestamps: false,
        hooks: {
            beforeValidate: function (obj) {
                let now = Date.now()
                if (obj.isNewRecord) {
                    obj.createAt = now
                    obj.updateAt = now
                    // obj.version = 0
                } else {
                    obj.updateAt = now
                    // ++obj.version
                }
            }
        }
    })
}
```

创建一个 models 文件夹，创建一个 goods 文件，把上面那个公共的定义模型方法进入，创建 goods 表格。

```javascript
var db = require('../db')
var Sequelize = require('sequelize')
// 创建表模型
const Goods = db.defineModel('goods', {
    id: {
        type: Sequelize.BIGINT(11),
        primaryKey: true,
        allowNull: false,
        unique: true,
        autoIncrement: true
    },
    name: Sequelize.STRING(50),
    img_src: Sequelize.STRING(100),
    price: Sequelize.FLOAT
})
// 同步表结构
Goods.sync()   // 如果表存在 不会刷新结构
Goods.sync({ force: true })   // 如果表存在 会删除表重新建表
module.exports = Goods
```

## 表插入数据

如果要操作 model 插入一些初始数据，可以直接用图形工具进行操作，也可以用 sequelize 来，还是在 goods 那个文件里面，添加9条数据进去。

```javascript
// 填充数据
for (let n=1;n<10;n++) {
    Goods.create({
        name:`good${n}`,
        img_src: `/static/imgs/good${n}.jpg`,
        price: Math.random()*100,
        status:1
    })
}
```

## 获取表数据

获取表数据主要用的是 findAll, find 这些 api，具体详情参见官网： [sequelize 官网](http://docs.sequelizejs.com/manual/tutorial/models-usage.html)
利用 koa 创建一个接口，在这个接口返回 goods 表的数据，有关于 koa 的这块这里就先不管了哈，下面有一个是获取列表数据，一个是获取单条数据。

```csharp
const goods = require('../models/goods')

module.exports = {
    'GET /goods/list': async (ctx, next) => {
        let data = await goods.findAll()
        ctx.body = data
    },
    'GET /goods/:id': async (ctx, next) => {
        let gid = ctx.params.id
        let data = await goods.findOne({ where: {id: gid} })
        ctx.body = data
    }
}
```

