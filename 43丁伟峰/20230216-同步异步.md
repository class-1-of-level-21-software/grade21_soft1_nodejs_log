## fs（文件系统 File System）
使用文件系统，需要先引入fs模块，fs是核心模块，直接引入不需要下载

fs模块中所有的操作都有两种形式可供选择（同步和异步）

同步文件会阻塞程序的执行，也就是除非操作完毕，否则不会向下执行代码
异步文件不会阻塞程序的执行，而是在操作完成时，通过回调函数将结果返回
文件的写入：

打开文件
向文件中写入内容
保存并关闭文件 

## 同步：
        1. 打开文件
```js
 fs.openSync(path,flags,[mode])
```
path 要打开文件的路径
flags 打开文件要做的操作类型
r 只读的
w 可写的
mode 可选的（设置文件的操作权限，一般不传）
该方法会返回一个文件的描述符作为结果，我们可以通过该描述符对文件进行各种操作

        2. 向文件中写入内容

fs.writeSync(fd,string,position,encoding)

fd 文件的描述符，需要传递要写入的文件的描述符
string 要写入的内容
position 写入的起始位置
encoding 写入的位置，默认为utf-8
3. 保存并关闭文件
```js
fs.closeSync(fd)

要关闭的文件的描述符
var fs = require("fs");
var fd = fs.openSync("hello.txt", "w");
fs.writeSync(fd, "今天天气真好");
fs.closeSync(fd);
```
## 异步：
1. 打开文件
```js
fs.open(path,flags,mode,callback)
```
用来打开一个文件
异步调用的方法，结果都是通过回调函数的参数返回的
回调函数的两个参数
err 错误对象，如果没有错误则为null
fd 文件的描述符
2. 向文件写入内容
```js
fs,write(fd,string,[position],[encoding],callback) 
```
用来异步写入一个文件
3. 关闭文件
```js
fs.close(fd,callback)
```
用来关闭文件 
```js
var fs = require("fs");
fs.open("hello.txt", "w", function (err, fd) {
    if (!err) {
        // 如果没有出错 则对文件进行写入操作
        fs.write(fd, "这是异步写入的内容", function (err) {
            if (!err) {
                console.log("写入成功");
            }
            fs.close(fd, function (err) {
                if (!err) {
                    console.log("文件已关闭");
                }
            })
        })
    } else {
        console.log(err);
    }
});
```