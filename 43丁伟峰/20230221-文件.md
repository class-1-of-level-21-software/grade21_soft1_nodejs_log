## 文件

### fs
在 Node.js 中读取文件最简单的方式是使用 fs.readFile() 方法，向其传入文件路径、编码、以及会带上文件数据（以及错误）进行调用的回调函数：
```js
const fs = require('fs')

fs.readFile('/Users/joe/test.txt', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  console.log(data)
})
```
另外，也可以使用同步的版本 fs.readFileSync()：

```JS
const fs = require('fs')

try {
  const data = fs.readFileSync('/Users/joe/test.txt', 'utf8')
  console.log(data)
} catch (err) {
  console.error(err)
}
```
fs.readFile() 和 fs.readFileSync() 都会在返回数据之前将文件的全部内容读取到内存中。

Node.js 提供一组类似 UNIX（POSIX）标准的文件操作API。 Node 导入文件系统模块(fs)语法如下所示：
```js
var fs = require("fs")

```

### 读取目录
语法
以下为读取目录的语法格式：
```js
fs.readdir(path, callback)
```
参数
参数使用说明如下：

path - 文件路径。

callback - 回调函数，回调函数带有两个参数err, files，err 为错误信息，files 为 目录下的文件数组列表。

实例
接下来我们创建 file.js 文件，代码如下所示：
```js
var fs = require("fs");

console.log("查看 /tmp 目录");
fs.readdir("/tmp/",function(err, files){
   if (err) {
       return console.error(err);
   }
   files.forEach( function (file){
       console.log( file );
   });
});
```