'use strict';

// 引入需要的工具包
const sp = require('superagent');
const cheerio = require('cheerio');

// 定义请求的URL地址
const BASE_URL = 'https://www.douban.com/';

// 1. 发送请求，获取HTML字符串
(async () => {
  let html = await sp.get(BASE_URL);
  
  // 2. 将字符串导入，使用cheerio获取元素
  let $ = cheerio.load(html.text);
  
  // 3. 获取指定的元素
  let books = []
  $('.movie-list list').each(function () {
    
    let info = {
      title: $(this).find('a').eq(0).attr('href'),
      name: $(this).find('a').eq(1).text(),
      image: $(this).find('img').attr('src')
    }
    books.push(info)
  })
  console.log(books)
})()