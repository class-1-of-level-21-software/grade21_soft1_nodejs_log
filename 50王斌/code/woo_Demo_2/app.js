'use strict';

const Koa = require('koa');
const controller = require('./controller');
const nun = require('./nunConfig');

let app = new Koa();

app.use(nun());



controller(app);

let port = 3000;
app.listen(port);
console.log(`服务运行于：http://localhost:${port}`);