'use strict' 

const nunjucks = require('nunjucks') 


let { noCache, watch } = require('./config') 

let env = nunjucks.configure('views', {
    autoescape: true,
    watch: watch,
    noCache: noCache
}) 

function fn() {
    return async (ctx, next) => {
        ctx.render = function (view, data) {
            ctx.body = env.render(view, data) 
        }
        ctx.kaixin = '寶兒姐' 
        await next() 
    }
}

module.exports = fn 
