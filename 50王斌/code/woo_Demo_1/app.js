'use strict'

const Koa = require('koa')
const bodyparser = require('koa-bodyparser')
const router = require('koa-router')


let app = new Koa()
let port = 8888

app.listen(port)

console.log(`Champion for setting in https://localhost:${port}`)


