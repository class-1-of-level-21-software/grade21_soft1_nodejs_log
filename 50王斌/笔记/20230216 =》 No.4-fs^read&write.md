# 周四 ——— FS GLOBAL PROCESS READ WRITE 

## global

啊......这个是一个 ‘全局变量’....呃.....或者‘全局对象’，不过这个只是在nodeJs里面这样称呼！！其实一般ta是叫window噢！


## FS_read 

即文件系统模块，负责读写文件。  

### 异步读文件

按照JavaScript的标准，异步读取一个文本文件的代码如下：

```
'use strict';

var fs = require('fs');

fs.readFile('sample.txt', 'utf-8', function (err, data) {
    if (err) {
        console.log(err);
    } else {
        console.log(data);
    }
});
```

### 同步读文件

除了标准的异步读取模式外，fs也提供相应的同步读取函数。同步读取的函数和异步函数相比，多了一个Sync后缀，并且不接收回调函数，函数直接返回结果。

用fs模块同步读取一个文本文件的代码如下：

```
'use strict';

var fs = require('fs');

var data = fs.readFileSync('sample.txt', 'utf-8');
console.log(data);
```


## WRITE 

WRITE _ 通过fs.writeFile()实现

```
'use strict';

var fs = require('fs');

var data = 'Hello, Node.js';
fs.writeFile('output.txt', data, function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('ok.');
    }
});
```



------



<font face="STCAIYUN" size=7>__.log__2023.0216</font>
------