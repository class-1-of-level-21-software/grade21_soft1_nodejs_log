# URL —— 路由~ 

今天给大家带来路由的用法！ 等同与我们家里的路由器， 但是又不完全啊等同与。 所以，下面我们细说
------



------




# 1.新内容——koa-router

------

首先我们现在package.json![show](./IMG/package.png)中添加依赖项
------

```

"koa-router": "12.0.0"

```

然后使用koa-router来处理URL！
------

其中要注意的是，require('koa-router')返回的是函数，即！
------

```
const router = require('koa-router')();
```

------


接下来，看成果![show](./IMG/0227%E4%BB%8A%E6%97%A5%E4%BB%BD%E7%AC%94%E8%AE%B0%E6%88%90%E6%9E%9C.png)
------



# MVC 的回顾

# 关于如何创建 MVC 项目


## 直接创建一个MVC项目

```
dotnet new mvc --no-https
```

## 创建一个文件夹并将MVC项目放进去

```
dotnet new mvc -n [项目名] --no-https
//-n 是 name 的意思
```


## MVC 

```
--M(model)模型


--V(view)视图


--C(controller)控制器
```



<font face="STCAIYUN" size=7>__.log__2023.0227</font>
------

