# 关于MySQL 8.0 版本的安装记录

## 1.下载地址！找到下载地址并进行下载~
MySQL官网：https://www.mysql.com/cn/
------
MySQL 8.0下载地址：https://dev.mysql.com/downloads/mysql/
------
如下:
------

![show](./IMG_MySQL%E9%99%90%E5%AE%9A%E7%89%88/a1.jpg)


可以看到的是，目前是我们的最新版本8.0.32（截止今日 2023.3.16 11:08AM）
------
以及两个版本，上面提供了压缩版本，直接解压配置就可以使用
------
如果需要下载 msi 格式文件（可以通过图形化界面操作），点击Go to Download Page > 按钮，这里使用msi格式文件下载
------






## 2.开始安装MySQL

okk,这里我们直接换一另一种安装方式好吧！
------



## 3.下载地址！找到下载地址并进行下载~


下载网址：https://dev.mysql.com/downloads/
------
选择这个
![show](./IMG_MySQL%E9%99%90%E5%AE%9A%E7%89%88/a1.png)
进入后选择这个
![show](./IMG_MySQL%E9%99%90%E5%AE%9A%E7%89%88/a2.png)
直接下载第一个
![show](./IMG_MySQL%E9%99%90%E5%AE%9A%E7%89%88/a3.png)





## 4.开始配置！

解压安装包到d盘（这里因人而异）
------
打开
------
![watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70](https://img-blog.csdnimg.cn/770ef7859e5c43fd83ca557cda7e7dca.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70)





编写MySQL配置文件
------
在解压目录下新建my.ini文件
------

![watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70](https://img-blog.csdnimg.cn/648a35f4427e48bd9ccb9c4a944befaa.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70)





将下面文本拷贝进my,ini文件中
------

```
[mysqld]
# 设置3306端口
port=3306
# 设置mysql的安装目录   ----------是你的文件路径-------------
basedir=D:\mysql-8.0.26-winx64\mysql-8.0.26-winx64
# 设置mysql数据库的数据的存放目录  ---------是你的文件路径data文件夹自行创建
#datadir=E:\mysql\mysql\data
# 允许最大连接数
max_connections=200
# 允许连接失败的次数。
max_connect_errors=10
# 服务端使用的字符集默认为utf8mb4
character-set-server=utf8mb4
# 创建新表时将使用的默认存储引擎
default-storage-engine=INNODB
# 默认使用“mysql_native_password”插件认证
#mysql_native_password
default_authentication_plugin=mysql_native_password
[mysql]
# 设置mysql客户端默认字符集
default-character-set=utf8mb4
[client]
# 设置mysql客户端连接服务端时默认使用的端口
port=3306
default-character-set=utf8mb4

```





初始化MySQL数据库
------
以管理员身份打开命令提示符
------
切换到bin目录下
------

![watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70](https://img-blog.csdnimg.cn/869a4bdba57044c0b65be96e5e0e260e.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70)




在MySQL目录下的bin目录下执行命令：
------

```
mysqld --initialize --console
```





![watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70](https://img-blog.csdnimg.cn/9df8256c4a1d4fc985e909eaf5d0c13b.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70)
我的随机密码是：2(eE8rwv#a(?
------
安装mysql服务并启动
------
```
mysqld --install mysql
```

```
net start mysql
```

连接数据库，并输入密码(密码是刚刚的随机密码)
------
```
mysql -uroot -p
```

修改密码,这里密码我设置为'root'
------
```
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root'
```

这里附带一句，可以输入quit或者exit都可以退出
------


## 5.配置环境变量

此电脑->属性->高级系统设置->环境变量->系统变量->新建变量
------

变量名：MYSQL_HOME
------

变量值：MySQL目录绝对路径
------


![watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70](https://img-blog.csdnimg.cn/2e6e9264fee14f9c9de653eedb79b809.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70)



![watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70](https://img-blog.csdnimg.cn/413634695022499ab978450af1756eaf.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70)
然后在系统变量里面找到path变量
------
添加
------

```
%MYSQL_HOME%\bin
```
![watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70](https://img-blog.csdnimg.cn/60a946d27ab84c69b95810936dde81c0.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2E4MDI5NzY=,size_16,color_FFFFFF,t_70)


点击确认 ！！即可！
------

最后的最后打开 services.msc  或者  右键此电脑->管理->服务与应用程序->服务 改为手动就可以了
------
不然开机自启直接拖慢电脑开机速度，真的会谢！
------

<font face="STCAIYUN" size=7>__.log__2023.0316</font>
------
