**Node.js**的web程序判断文件、文件夹
===
 当我们创建了一个文件服务器，可以更多的扩展，去实现的我们所需要的结果，像是 **开发web程序** 。因为用 **node.js** 去创建服务器，所以只用去掌握好***JavaScript语言***，因为拥有异步执行，也可以减少等待代码执行的时间，只用等代码的返回

创建服务器
---
+ 先用node.js去创建服务器
    > 创建一个main.js，在里面实现服务器：
    ```js
    'use strict'

        let http = require('http'),
            fs = require('fs');
        
        let server = http.createServer(
            function(request,response){
                response.writeHead(200,{"content-type":"text/html;charset=utf-8"});
                response.end('Hello world!');
            }
        ).listen(8080);

        console.log('Server is runing at http://localhost:8080');
    ```
    > 这样就简单的创建好了一个 **web程序** ，接下来实现文件和文件夹的判断

判断文件和文件夹
---
+ 我们要引用到**fs 模块**，去使用 ***stat.isDirectory***() 方法去*判断文件夹和文件，我们来简单的用代码实现一下*

    > 创建一个文件夹 *Project*，再创建 *main.js* 和 *index.html* 文件，利用代码去实现判断：
    ```js
    'use strict'

        let fs = require('fs'),
            http = require('http');

        let server = http.createServer(
            function(request,response){
                
                let path ='.'+request.url;            
                fs.stat(path,function(err,stats){
                    if(!err){
                        if(path == './'){
                            response.writeHead(200,{"content-type":"text/html;charset=utf-8"});
                            console.log(path);
                            response.end('可以判断文件或文件夹');
                        } 
                        else if(stats.isDirectory()==true){
                            response.writeHead(200,{"content-type":"text/html;charset=utf-8"});
                            console.log(path);
                            response.end('这是文件夹');
                        }
                        else{
                            response.writeHead(200,{"content-type":"text/html;charset=utf-8"});
                            console.log(path);
                            response.end('这是文件');
                        }
                    }
                    else{
                        response.writeHead(404,{"content-type":"text/html;charset=utf-8"});
                        console.log(path);
                        response.end('不存在该文件、文件夹或不是文件和文件夹');
                    }
                });
            }
        ).listen(8080);

        console.log('http://localhost:8080');
    ```
    > 像这样开发了一个判断文件夹和文件的**web程序**