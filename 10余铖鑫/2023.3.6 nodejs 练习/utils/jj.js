'use strict'
// 创建fs文件系统模块
const fs = require('fs')
// 创建一个函数reg，来读取./xixihaha文件夹里面的所有js文件
function reg(obj){
    // 获取一个行参，如果obj为空传则是./xixihaha，如果有传值则是值的地址
    obj = obj || './xixihaha'
    //声明一个router用来同步读取obj中的文件内容
    let router = fs.readdirSync(obj)
    //把读取到的文件内容返回给函数，过滤一下把router.js文件排除
    return router.filter(item=>{
        return item != 'router.js'
    })
}
function que(path){
}

module.exports = {
    reg,
    que
}