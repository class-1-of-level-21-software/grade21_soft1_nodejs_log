'use strict'

const { DataTypes } = require('sequelize');

module.exports={
    id:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    name:DataTypes.STRING,
    emali:DataTypes.STRING
}