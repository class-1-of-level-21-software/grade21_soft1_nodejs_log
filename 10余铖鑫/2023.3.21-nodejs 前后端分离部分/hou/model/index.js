'use strict'

const {Sequelize,Datatypes} = require('sequelize')

const {fin} = require('../utils/rou')

let sql = new Sequelize('db1','root','root',{
    host:'120.79.13.55',
    dialect:'mysql'
})

let obj = {}

let files = fin('./model');

files.forEach(file => {
    let tmpobj = require('../model/' + file);
    let name = file.replace('.js','');
    let lowName = name.toLowerCase();
    obj[name] = sql.define(lowName,tmpobj)
});

obj.sync = async(force) => {
    if(force){
        return sql.sync({ force :true});
    }else{
        return sql.sync();
    }
}


module.exports = obj