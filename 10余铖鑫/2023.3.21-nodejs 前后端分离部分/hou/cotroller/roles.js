'use strict'

const {role} =require('../model')

async function getAll(ctx,next){
    ctx.body = await role.findAll()
}
async function getByID(ctx,next){
    let id = await ctx.params.id
    let data = await role.findAll({
        where:{
            id:id
        }
    })
    ctx.body={
        code:1000,
        data:data,
        msg:'查询指定项目成功'
    }
}
async function add(ctx,next){
    let obj = ctx.request.body
    await role.create({
        name:obj.name,
        emali:obj.emali
    })
    ctx.body={
        code:1000,
        data:null,
        msg:'新增成功'
    }
}
async function update(ctx,next){
    let id = ctx.request.params.id
    let obj = ctx.request.body
    await role.update({name:obj.name,email:obj.email},{
        where:{
            id:id
        }
    })
    ctx.body={
        code:1000,
        data:null,
        msg:'更新成功'
    }
}
async function del(ctx,next){
    let id = ctx.request.params.id
    await role.destroy({
        where:{
            id:id
        }
    })
    ctx.body={
        code:1000,
        data:null,
        msg:'删除成功'
    }
}

module.exports = {
    'get /roles': getAll,
    'get /roles/:id': getByID,
    'post /roles': add,
    'put /roles/:id': update,
    'del /roles/:id': del,
}