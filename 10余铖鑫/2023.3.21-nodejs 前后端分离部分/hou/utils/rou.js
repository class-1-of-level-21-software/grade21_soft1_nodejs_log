'use strict'
const fs = require('fs')

function fin(paht){
    paht = paht || './cotroller'
    let duqu = fs.readdirSync(paht)
    return duqu.filter(filename => {
        return filename.endsWith('.js') && filename!=='index.js'
    }); 
}


function zc(files,router){
    files.forEach(file => {
        let tmpModule = require('../cotroller/' + file);
        for (let key in tmpModule) {
            let tmpArr = key.split(' ');

            let method = tmpArr[0];
            let url = tmpArr[1];
            let fn = tmpModule[key];

            if (method === 'get') {
                router.get(url,fn);
            } else if (method === 'post') {
                router.post(url,fn);
            } else if (method === 'put') {
                router.put(url,fn);
            } else if (method === 'del') {
                router.delete(url,fn);
            }
        }
    })
}

module.exports = {
    fin,
    zc
}