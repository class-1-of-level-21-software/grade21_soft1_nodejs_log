认识**node.js**和安装
===

一 .认识**Node.js**
---
### 1.什么是**Node.js** :
   + 就是运行在服务端的**JavaScript**
    
     Node.js 是一个基于 *Chrome JavaScript* 运行时建立的一个平台  
     Node.js 是一个事件驱动 I/O 服务端 **JavaScript** 环境，基于 **Google 的 V8 引擎，V8 引擎执行 Javascript 的速度非常快，性能非常好**
        + 如果你在了解**Node.js**之前有JavaScript的基础，在使用时就会大大提高学习效率，这对于**后端**的服务部署**提高了性能**
### 2.**Node.js**的发展历程 :
1. **混沌期**：发布初期，创始人 *Ryan Dahl* 带著他的团队开发出了**以 Web 为中心的**“**Web.js**”，一切都是非常混乱，API大多都还除外研究阶段s
2. **成长期**：**Node.js** 的核心用户 *Isaac Z. Schlueter* 开发出奠定了 **Node.js** 如今地位的重要工具--**npm**。同时也为他后来成为 *Ryan* 的接班人的重要条件
3. **高速期**：**connect, express, socket.io** 等库的出现吸引了一大波爱好者加入到 **Node.js** 开发者的阵营中来。**CoffeeScript** 的出现更是让不少 *Ruby* 和 *Python* 开发者找到了学习的理由。期间一大波以 **Node.js** 作为运行环境的 CLI 工具涌现，其中不乏有用于加速前端开发的优秀工具，如   **less, UglifyJS, browserify, grunt** 等等。**Node.js** 的发展势如破竹
4. **更迭期**：经过了一大批一线工程师的探索实践后，**Node.js** 也开始进入了时代的更迭期，新模式代替旧模式，新技术代替旧技术，好实践代替旧实践。ES6 也开始出现在 **Node.js** 世界中
5. **分裂期**：ES6 的发展越来越明显，v8 也对 ES6 中的部分特性实现了支持，如 **Generator** 等等，利用--**harmony**作为开启阀门。后来，诞生了 **Io.js** 分支，再后来也回到了 **Node.js** 主线上
6. **飞速发展期**：随著 ES2015 的发展和最终定稿，一大批利用 ES2015 特性开发的新模块出现，如原 **express** 核心团队所开发的 **koa**

    > 作者：时见疏星  
    > 链接：https://www.jianshu.com/p/5b9b245fcefa  
    > 来源：简书

### 3.**Node.js**的作用 :
+ **node.js**可以大大提升了开发的性能以及便利. 我们知道 **Apache + PHP** 以及 **Java** 的 **Servlet** 都可以用来开发动态网页，**Node.js** 的作用与他们类似，只不过是使用 **JavaScript** 来开发，它大大提升了开发的性能以及便利
+ 使用 ***node*** 开发还可以使用配套的**npm**包管理工具 **NPM**是随同**NodeJS**一起安装的包管理工具，能解决**NodeJS**代码部署上的很多问题

二 .安装**Node.js**
---
+ 点击这个**链接[Node.js下载安装](https://nodejs.org/en/download/)**就可以去**官网**找寻你需要的相应版本，进行下载安装
![官网下载地址截图](./imgs/Node.js%E4%B8%8B%E8%BD%BD/Node%E5%AE%98%E7%BD%91%E4%B8%8B%E8%BD%BD%E5%9C%B0%E5%9D%80.png)
    + . 安装**Node.js**之后，可以在终端中打入```node -v```查看版号        
        >   node -v  
        >   18.02.5  
        + 类似这个样子,查看版号，就知道是否是你需要的了
