**Node.js**的web程序判断文件、文件夹 *2*
===
用**Node.js**去做一个***文件、文件夹的判断程序***，在判断文件和文件夹时，我们往往也会将文件的内容也显示出来，当然也有多种方法去实现我们想要的效果，有*同步、promise和其他方法！*
  
一 .判断文件、文件夹的同步方法：
---
+ 同步方法的文件、文件夹判断，代码是比较简单的，但是在遇到一些大文件的读写时，就会大大增加文件读写成功后返回相应的速度，所以一般在处理一些小文件时，还是可以的的，代码量也不是很大
    > 创建一个main.js文件，去写我们的同步方法代码：
    ```js
    'use strict';

    let fs = require('fs'),
        http = require('http');

        function readFile(path){
            let result = 404;

            let stats = fs.statSync(path);
            
            if(stats.isDirectory()){
                let newPath = path+'index.html';
                let data = fs.readFileSync(newPath,'utf-8');
                result = data;
            }
            return result;
        }

        let server = http.createServer(function(request,response){
            let path = '.'+request.url;
            let datas = readFile(path);
            response.end(datas);
        }).listen('8080');

        console.log("server is runing at http://localhost:8080");
    ```    
    > 虽然是同步方法，代码很简单，但是功能性是很差的

二 .用promise方法判断文件、文件夹：
---
+ 用promise方法，就可以使得异步执行成功，但会加大代码的编写难度，但是会提升响应速度
 
三 .用async/await去判断文件、文件夹：
---
+ async/await这个东西其实是跟promise一样，并且不用像promise还要实例一下，用起来还是很方便的