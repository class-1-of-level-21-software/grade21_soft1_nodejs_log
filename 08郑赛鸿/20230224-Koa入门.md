# 认识Koa

非常流行的Node Web服务器框架就是Koa。

Koa官方的介绍：

koa：next generation web framework for node.js；
koa：node.js的下一代web框架；
事实上，koa是express同一个团队开发的一个新的Web框架：

目前团队的核心开发者TJ的主要精力也在维护Koa，express已经交给团队维护了；
Koa旨在为Web应用程序和API提供更小、更丰富和更强大的能力；
相对于express具有更强的异步处理能力（后续我们再对比）；
Koa的核心代码只有1600+行，是一个更加轻量级的框架，我们可以根据需要安装和使用中间件；

## koa初体验

创建koa2工程

首先，我们创建一个目录hello-koa并作为工程目录用VS Code打开。然后，我们创建app.js，输入以下代码：
```
// 导入koa，和koa 1.x不同，在koa2中，我们导入的是一个class，因此用大写的Koa表示:
const Koa = require('koa');

// 创建一个Koa对象表示web app本身:
const app = new Koa();

// 对于任何请求，app将调用该异步函数处理请求：
app.use(async (ctx, next) => { 
    await next();
    ctx.response.type = 'text/html';
    ctx.response.body = '<h1>Hello, koa2!</h1>';
});

// 在端口3000监听:
app.listen(3000);
console.log('app started at port 3000...'
```