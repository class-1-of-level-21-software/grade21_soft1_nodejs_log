# 第一个Node程序

可以在命令提示符写node 看到>就是在交互式环境

例如，在Node交互式环境下，输入：
```
> 100 + 200 + 300;
600
```
直接可以看到结果600。

如果在桌面的一个js文件里 写 内容如下
```
100+600+200
```
然后命令行模式下执行
```
C:/users/Adminnistator/desktop/mm.js
```
可以看到什么输出都没有

如果想要输出结果，就比如用console.log()打印出来，所以改造一下就是
```
console.log(100+600+200)
```
可以看到结果
```
C:/users/Adminnistator/desktop/mm.js
900
```


## module.exports

很多时候，你会看到，在Node环境中，有两种方法可以在一个模块中输出变量：

方法一：对module.exports赋值：
```
// hello.js

function hello() {
    console.log('Hello, world!');
}

function greet(name) {
    console.log('Hello, ' + name + '!');
}

module.exports = {
    hello: hello,
    greet: greet
};
```