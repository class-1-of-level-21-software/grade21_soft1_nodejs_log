# Nodejs新特性async await 的使用

1.Nodejs新特性async await 的使用
以前异步处理方式，通过回调函数来获取异步方法数据。
```
function getData(callback) {
    setTimeout(function(){
        var name = "小红"
        callback(name)
    },1000)
}

getData(function (name){
    console.log(name);//小红
})
ES6出来可以通过Promise来进行异步处理
```

// 方式1
```
var p = new Promise(function (resolve, reject) {
    // resolve 表示执行成功
    // reject 表示执行失败
    setTimeout(() => {
        var name = "小明";
        resolve(name)
    })
})

p.then( (data) => {
    console.log(data);// 小明
})
// 方式2 ，封装getData函数
function getData(resolve, reject) {
    setTimeout(function (){
        var name = "Tom"
        resolve(name);
    }, 1000)
}
var p = new Promise(getData)

p.then((data)=> {
    console.log(data); //Tom
})
```
async 是异步简写 ， 而await可以认为是async wait的简写，所以应该很好理解async用于申明一个异步的function，而await用于等待一个异步方法执行完成。

简单理解：async 是让方法变成异步，await是等待异步方法执行完毕。
```
async function test () {
    return "hello world!"
}
console.log(test())// Promise { 'hello world!' }
async function main () {
    var data = await test();
    console.log(data);// hello world!
}
main()
```
示例2
```
async function test () {
    return new Promise((resolve, reject) => {
        setTimeout(function () {
            var name = "Lucy";
            resolve(name);
        }, 1000)
    })
}


async function main () {
    var data = await test();
    console.log(data);//Lucy
}
main()
```
练习: 获取当前路径下为文件目录的文件名
```
var fs = require("fs")


async function isDir(path) {
    return new Promise((resolve, reject)=> {
        fs.stat(path, (err,stats)=> {
            if (err) {
                console.log(err);
                reject(err);
                return
            }
            if (stats.isDirectory()) {
                resolve(true);
            }else {
                resolve(false);
            }
        })
    })
}

function  main() {
    var path = "../01.node"
    var dirArr = []
    fs.readdir(path, async (err, data)=> {
        if (err) {
            console.log(err);
            return
        }
        for (var i=0;i<data.length;i++){
            if (await isDir(path + '/' + data[i])) {
                dirArr.push(data[i])
            }
        }
        console.log(dirArr)
    })
}

main()
```