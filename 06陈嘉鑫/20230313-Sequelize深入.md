# Sequelize
```js
'use strict'

const {Sequelize,DataTypes}=require('sequelize');



let db=new Sequelize('db1','root','root',{
    host:'localhost',
    dialect:'mysql'
})

let Role=db.define('Roles',{
    id:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    username:DataTypes.STRING
})

Role.sync().then(()=>{
    Role.create({
        username:'jjjj'
    });
    Role.findAll().then(rows=>{
        rows.forEach(item=>{
            console.log(item.dataValues);
        })
    })
})

Role.sync().then(()=>{
    Role.create({
        username:'哈哈'
    }).then(()=>{
        Role.findAll().then(item=>{
            item.forEach(Element=>{
                let createdTime=new Date(Element.dataValues.createdAt)
                let updatedTime=new Date(Element.dataValues.updatedAt)
                console.log(`id:${Element.dataValues.id} username:${Element.dataValues.username} createdTime:${createdTime.getTime()} updatedTime:${updatedTime.getTime()}`);
            })
        })
    })
})


```