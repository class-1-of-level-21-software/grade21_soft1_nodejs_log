# 重构
```
现在，我们已经可以处理不同的URL了，但是看看app.js，总觉得还是有点不对劲。

所有的URL处理函数都放到app.js里显得很乱，而且，每加一个URL，就需要修改app.js。随着URL越来越多，app.js就会越来越长。

如果能把URL处理函数集中到某个js文件，或者某几个js文件中就好了，然后让app.js自动导入所有处理URL的函数。这样，代码一分离，逻辑就显得清楚了。
```
```
1.建一个controllers文件夹
2.里面建各种类型的js
3.然后建一个主要js为导出的
```
```
代码如下：
const router = require('koa-router');
const fs = require('fs');
const bodyParse = require('koa-bodyparser');

function findController(path){
    path = path || './controllers';
    let files = fs.readdirSync(path);
    return files.filter(item=>{
        return item !== 'index.js';
    })
}

function registryRouter(obj){
    for(key in obj){
        let tmpArr = key.split(' ');
        let method = tmpArr[0];
        let url = tmpArr[1];
        let fn = obj[key];
        if(method==='get'){
            router.get(url,fn);
        }
    }
}

function processRouter(app){
    let files = findController();
    files.forEach(element => {
        let tmpModule = require('./'+element.replace('.js',''));
        registryRouter(tmpModule);
    });
    app.use('koa-router');
    app.use('koa-bodyparse');
}
module.exports = processRouter;
```
```
最后用外面js引入
const Koa = require('koa');
let app = new Koa();
let Controllers = require('./controllers');
Controllers(app);
app.listen(6000);

```