# 封装
```js
'use strict';

const fs=require('fs')
const {Sequelize,DataTypes}=require('sequelize');

//连接数据库
let db=new Sequelize('db1','root','root',{
    host:'localhost',
    dialect:'mysql'
});

//找到model文件夹下除了index.js外的所有模型
function findFiles(path){
    path=path || './model'
    let files=fs.readdirSync(path)
    // console.log(files);
    return files.filter(item=>{
        return item.endsWith('.js') && item !=='index.js'
    })
}

let obj={};
//遍历定义模型
function defineModel(files){
    files.forEach(element => {
        let tmpMOdel=require('../model/'+element);
        let modelName=element.replace('.js','');
        let lowModelName=modelName.toLowerCase();
    
        obj[modelName]=db.define(lowModelName,tmpMOdel,{
            tableName:'app'+lowModelName
        })
    });
    
}

let files=findFiles();
console.log(files);
defineModel(files);

obj.sync=async function(){
    return db.sync({force:true})
}

module.exports=obj
```