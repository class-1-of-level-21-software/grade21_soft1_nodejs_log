# 浏览器上渲染
## index.html
```
{{name}}
```
## js模块
```
const Koa = require('koa');
let app = new Koa();
const nunjucks = require('nunjucks');
nunjucks.configure({
    noCache:true,
})
app.use(async (ctx,next)=>{
    ctx.body = = nunjucks.render('index.html',{name:'cjx'});
})
 
app.listen(10000)
```
## 显示
```
登入localhost：10000就能看见cjx
```

