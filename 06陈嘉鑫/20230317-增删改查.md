# 增删改查
```js
//controller文件夹下的roles.js
const fs=require('fs')
const model=require('../model');



let url='../statics/img/IU-LILAC.jpg';



let list=[
    {
        id:1,
        name:'哈哈',
        img:url
    },{
        id:2,
        name:'嘻嘻',
        img:url
    },{
        id:3,
        name:'呵呵',
        img:url
    },{
        id:4,
        name:'嘿嘿',
        img:url
    },{
        id:5,
        name:'呐呐',
        img:url
    },
]




async function getAll(ctx,next){
    console.log(model);
    let data=await model.Role.findAll();
    console.log(data);

    ctx.render('roles.html',{list:data})
}
async function getById(ctx,next){
    let id =ctx.params.id;
    let data=await model.Role.findAll({
        where:{
            id:id
        }
    })
    ctx.render('roles.html',{list:data})

}
async function addItem(ctx,next){
    let obj=ctx.request.body;
    console.log(obj);

    await model.Role.create({
        rolename:obj.rolename
    })

    ctx.body='新增成功'
}
async function updateItem(ctx,next){

    // let id=ctx.params.id;
    // let rolename=ctx.params.rolename;
    let id=ctx.request.body.id;
    let rolename=ctx.request.body.rolename;

    console.log(id,rolename);
    await model.Role.update({rolename:rolename},{
        where:{
            id:parseInt(id)
        }
    })

    
    
}
async function delItem(ctx,next){
    let id=ctx.params.id
    console.log(id);
    await model.Role.destroy({
        where :{
            id:id
        }
    })
    ctx.body='删除成功'
}
async function findImg(ctx,next){
    ctx.type='image/jpeg';
    ctx.body=fs.readFileSync('./statics/img/IU-LILAC.jpg')
}

async function alterRoles(ctx,next){
    let id=ctx.params.id;
    let rolename=ctx.params.rolename;
    let data={
        id:id,
        rolename:rolename
    }
    
    ctx.render('alterRoles.html',{data:data})
    
}

module.exports={
    'get /roles':getAll,
    'get /roles/:id':getById,
    'get /alterRoles/:id/:rolename':alterRoles,
    'post /roles':addItem,
    'put /roles/:id':updateItem,
    'delete /roles/:id':delItem,
    'get /statics/img/IU-LILAC.jpg':findImg
}
```
```html
<!-- roles.html -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table>
        <tr>
            <th>编号</th>
            <th>角色名称</th>
            <th>操作</th>
        </tr>
        {% for item in list %}

            <tr id="{{item.id}}">
                <td>{{item.id}}</td>
                <td>{{item.rolename}}</td>
                <td>
                    <input type="button" value="编辑" onclick="alter('{{item.id}}','{{item.rolename}}')">
                    <input type="button" value="删除" onclick="remove('{{item.id}}')">
                </td>
            </tr>
        {% endfor %}
    </table>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="/statics/js/roles.js"></script>
</body>
</html>

<!-- alterRoles.html -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table>
        <tr>
            <th>编号</th>
            <th>角色名称</th>
        </tr>
        <tr>
            <td id="id">{{data.id}}</td>
            <td><input type="text" name="" id="rolename" value="{{data.rolename}}"></td>
        </tr>
        <tr>
            <td>
                <input type="button" value="save" onclick="save('{{data.id}}')">
            </td>
            <td>               
                <input type="button" value="back" onclick="cancel()">
            </td>
            
        </tr>
    </table>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="/statics/js/roles.js"></script>
</body>
</html>
```

```js
//给html的onclick用的方法们
function remove(id){  
    if (confirm(`确认删除id为${id}的角色吗`)) {
        axios.delete('/roles/'+id).then(()=>{
            let getById=$('#'+id);
            // console.log(getById);
            getById.remove()
        })
    }
}

function alter(id,rolename){
    console.log(id,rolename);

    location.href='/alterRoles/'+id+'/'+rolename
    
}

function cancel(){
    console.log('我是返回');
    location.href='/roles';
}

function save(id){
    console.log('我是保存');
    let rolename=$('#rolename').val()
    let obj={
        id:id,
        rolename:rolename
    }
    console.log(obj);
    if (confirm(`确认修改id为${id}的角色吗`)) {
        axios.put('/roles/'+id,obj)
            
    }
}
```